﻿using System;

namespace task6
{
    class Program
    {
        static void Main(string[] args)
        {
            float height;
            float weight;
            float BMI;
            bool isANumber = false;

            Console.WriteLine("Hello there, it is time to check your BMI!");
            Console.Write("For starters, what is your height (in cm)? ");
            isANumber = float.TryParse(Console.ReadLine(), out height);
            while (!isANumber)
            {
                Console.WriteLine("I am sorry, that is not a valid number. Could you please enter a valid number for your height (in cm)? ");
                isANumber = float.TryParse(Console.ReadLine(), out height);
            }
            Console.Write("OK, that is good, your height is " + height + ". And what is your weight (in kg)? ");

            isANumber = float.TryParse(Console.ReadLine(), out weight);
            while (!isANumber)
            {
                Console.WriteLine("I am sorry, that is not a valid number. Could you please enter a valid number for your weight (in kg)? ");
                isANumber = float.TryParse(Console.ReadLine(), out weight);
            }
            Console.WriteLine("Your weight is " + weight + ".");

            BMI = weight / MathF.Pow((height / 100), 2);
            switch (BMI)
            {
                case float n when (n < 18.5):
                    Console.WriteLine("Your BMI of " + BMI.ToString("0.00") + " is considered to be: underweight");
                    break;

                case float n when (n >= 18.5 && n < 25):
                    Console.WriteLine("Your BMI of " + BMI.ToString("0.00") + " is considered to be: normal weight");
                    break;

                case float n when (n >= 25 && n < 30):
                    Console.WriteLine("Your BMI of " + BMI.ToString("0.00") + " is considered to be: overweight");
                    break;

                case float n when (n >= 30):
                    Console.WriteLine("Your BMI of " + BMI.ToString("0.00") + " is considered to be: obese");
                    break;

                default:
                    Console.WriteLine("There was something wrong with the BMI. It is registered as: " + BMI.ToString("0.00"));
                    break;
            }

        }
    }
}
