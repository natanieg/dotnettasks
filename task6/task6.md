# Assignment 6: BMI Calculator
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task6

# Assignment text
• Write a program that allows the
user to input two values:
• Weight & Height
• It must calculate BMI and then
categorise the result:
• Underweight: BMI is less than 18.5
• Normal weight: BMI is 18.5 to 24.9
• Overweight: BMI is 25 to 29.9
• Obese: BMI is 30 or more