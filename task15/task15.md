# Assignment 15: Opening a file
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task15

# Assignment text
- Create a GUI form that can open a .txt file from the drive and display the content.
	• Optionally:
	• add search features
	• add a dialog box for selecting the file to open
	• display file properties