﻿namespace task15
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labelAllTextFromFile = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnSearchForFilename = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Text from file:";
            // 
            // labelAllTextFromFile
            // 
            this.labelAllTextFromFile.AutoSize = true;
            this.labelAllTextFromFile.Location = new System.Drawing.Point(176, 69);
            this.labelAllTextFromFile.Name = "labelAllTextFromFile";
            this.labelAllTextFromFile.Size = new System.Drawing.Size(99, 17);
            this.labelAllTextFromFile.TabIndex = 1;
            this.labelAllTextFromFile.Text = "<Placeholder>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "File name of text file:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(192, 26);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(124, 22);
            this.textBox1.TabIndex = 3;
            // 
            // btnSearchForFilename
            // 
            this.btnSearchForFilename.Location = new System.Drawing.Point(382, 26);
            this.btnSearchForFilename.Name = "btnSearchForFilename";
            this.btnSearchForFilename.Size = new System.Drawing.Size(75, 23);
            this.btnSearchForFilename.TabIndex = 4;
            this.btnSearchForFilename.Text = "Search";
            this.btnSearchForFilename.UseVisualStyleBackColor = true;
            this.btnSearchForFilename.Click += new System.EventHandler(this.BtnSearchForFilename_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSearchForFilename);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelAllTextFromFile);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelAllTextFromFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnSearchForFilename;
    }
}

