﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task15
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]

        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());


        }

        /*static void PopulateAllTextFromFileField()
        {

            string filePath = @"C:\Users\nagaso\Documents\Experis\Git\dotnettasks\task15\Example.txt";
            string[] fileContent;

            if (File.Exists(filePath))
            {
                fileContent = File.ReadAllLines(filePath);
                foreach (string text in fileContent)
                {
                    Console.WriteLine(text);
                }
            }
            else
            {
                Console.WriteLine("Could not find file <" + Path.GetFileName("Example.txt") + "> in: " + filePath);
            }
        }*/
    }
}
