﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task15
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            //PopulateAllTextFromFile();
            InitializeComponent();
            this.labelAllTextFromFile.Text = PopulateAllTextFromFile(@"Example.txt");
        }

        static private string PopulateAllTextFromFile(string file)
        {
            //string filePath = @"Example.txt";// @"C:\Users\nagaso\Documents\Experis\Git\dotnettasks\task15\Example.txt";
            string[] fileContent;

            if (File.Exists(file))
            {
                fileContent = File.ReadAllLines(file);
                string textFromFile = "";
                foreach (string text in fileContent)
                {
                    textFromFile += text + '\n';
                }
                return textFromFile;
            }
            else
            {
                return ("Could not find file <" + file + ">. ");
            }
        }

        private void BtnSearchForFilename_Click(object sender, EventArgs e)
        {
            this.labelAllTextFromFile.Text = PopulateAllTextFromFile(textBox1.Text);
        }
    }
}
