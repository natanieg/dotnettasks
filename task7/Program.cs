﻿using System;

namespace Task_7
{
    class Program
    {
        static void Main(string[] args)
        {
            // Variables
            int maxNumber = 4000000;
            int oldFibonacciNumber = 0;
            int currentFibonacciNumber = 1;
            int newFibonacciNumber;
            int sumOfEvenNumbers = 0;

            Console.WriteLine("Created by: Nataniel Gåsøy and Øyvind Mathisen");

            // Generate the Fibonacci sequence
            while (currentFibonacciNumber < maxNumber)
            {
                // Generating the next number in the sequence.
                newFibonacciNumber = oldFibonacciNumber + currentFibonacciNumber;

                // Checking if the current number in our sequence is even, to add it to the total.
                if (currentFibonacciNumber % 2 == 0)
                {
                    sumOfEvenNumbers += currentFibonacciNumber;
                }

                // Shifting the sequence further down.
                oldFibonacciNumber = currentFibonacciNumber;
                currentFibonacciNumber = newFibonacciNumber;
            }

            Console.WriteLine("Total: {0}", sumOfEvenNumbers);

            // Terminating program
            Console.WriteLine("\nPress any non-function key to terminate the program.");
            Console.ReadKey();
        }
    }
}
