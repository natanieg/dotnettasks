# Assignment 17: RPG DB - Design
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task17

# Assignment text
- Based on the class structure that you built for your character generator UI (Use you own .DLL or the other - your choice)
	• Create a database with appropriate tables to store data for those objects (SQLite)
	• Write SQL statements for SELECT, INSERT, UPDATE, & DELETE
	• The statements must be stored in a text file
	• Upload only the text file