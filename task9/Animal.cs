﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task9
{
    class Animal
    {
        public string name { get; set; }
        public string latinName { get; set; }
        public enum locomotion
        {
            walk,
            fly,
            swim
        }
        public locomotion movement { get; set; }

        public Animal()
        {

        }

        public Animal(string name)
        {
            this.name = name;
        }

        public Animal(string name, string latinName) : this(name)
        {
            this.latinName = latinName;
        }

        /// <summary>
        /// Constructor for a new animal
        /// </summary>
        /// <param name="name">Name of the animal</param>
        /// <param name="latinName">Latin/scientific name</param>
        /// <param name="loc">The movement type of the animal (walk, fly, swim)</param>
        public Animal(string name, string latinName, locomotion loc) : this(name, latinName)
        {
            this.movement = loc;
        }
    }
}
