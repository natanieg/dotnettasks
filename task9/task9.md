# Assignment 9: Animal Park
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task9

# Assignment text
• Create an Animal class
	- Provide properties
	- Provide behaviours
	- Provide an overloaded constructor
• Create multiple Animal objects and assign the
appropriate properties to represent at least three
different animals
• Store them in a Collection of your choice