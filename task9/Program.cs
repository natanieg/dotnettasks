﻿using System;
using System.Collections.Generic;

namespace task9
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();

            Animal dog = new Animal("Dog");
            Animal hamster = new Animal("Hamster", "Cricetinae");
            Animal cat = new Animal("Cat", "Felis catus", Animal.locomotion.walk);

            animals.Add(dog);
            animals.Add(hamster);
            animals.Add(cat);

            foreach (Animal animal in animals)
            {

                Console.WriteLine(animal.name);
            }
        }
    }
}
