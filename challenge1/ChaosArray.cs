﻿using System;
using System.Collections.Generic;
using System.Text;

namespace challenge1
{
    class ChaosArray : List<object>
    {
        public int listLength = 20;

        public ChaosArray() : base()
        {
        }

        public void InsertObject(object obj)
        {
            Random random = new Random();
            try
            {
                int randomNumber = random.Next(0, listLength);
                if (this[randomNumber] == null)
                {

                    this.Insert(randomNumber, obj);
                }
                else
                {
                    Console.WriteLine("I am sorry, but index number " + randomNumber + " is already in use. ");
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        public object RetrieveObject()
        {
            Random random = new Random();
            int randomNumber = random.Next(0, listLength);
            Console.WriteLine("Fetching object with index number: " + randomNumber + "... ");
            try
            {
                //for (int i = 0; i <= 4; i++)
                //{
                //    Console.WriteLine(" --- " + this[i].GetType() + "  --- " + i + "---" + this[i] + "\n");
                //}
                if (this[randomNumber] != null)
                {
                    return this[randomNumber];
                }
                else
                {
                    Console.WriteLine("I am sorry, but there is nothing saved in index number " + randomNumber + ". " + this[randomNumber]);
                    return null;
                }
            }
            catch (NullReferenceException nullRef)
            {
                Console.WriteLine("I am sorry, but there is nothing saved in index number " + randomNumber + "\n\n" + nullRef + "\n\n");
                return null;
            }

            catch (ArgumentOutOfRangeException rangeExp)
            {
                Console.WriteLine("I am sorry, but there is nothing saved in index number " + randomNumber + "\n\n" + rangeExp + "\n\n");
                return null;
            }
        }

        public void PopulateChaosArray()
        {
            Console.WriteLine("Populating list");
            for (int i = 0; i < listLength; i++)
            {
                this.Insert(i, null);
            }

            for (int i = 1; i < listLength / 5; i++)
            {
                Random random = new Random();
                int randomNumber = random.Next(0, listLength);
                this.Insert(randomNumber, i * 48);
            }

            for (int i = 1; i < listLength / 5; i++)
            {
                Random random = new Random();
                int randomNumber = random.Next(0, listLength) % 128;
                char c = Convert.ToChar(randomNumber);
                this.Insert(c, i * 33 % listLength);
            }

            this.Insert(0, "Start of the array. ");
            this.Insert(1, 47);
            this.Insert(2, 'B');
            List<int> intList = new List<int>();
            intList.Add(54);
            intList.Add(88);
            intList.Add(12);
            this.Insert(3, intList);
            this.Insert(listLength - 1, "End of the line. ");
        }

    }
}
