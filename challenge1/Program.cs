﻿using System;

namespace challenge1
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice = 0;
            ChaosArray chaosArray = new ChaosArray();
            chaosArray.PopulateChaosArray();

            while (choice != 9)
            {
                Console.WriteLine("\n--------------------------------------------\n" +
                    "\nHello there, would you like to add or retrieve something?" +
                    "\n1: Add. " +
                    "\n2: Retrieve. " +
                    "\n\n9: Exit. ");
                choice = ValidateNumberInput();

                switch (choice)
                {
                    case 1:
                        AddObject(chaosArray);
                        break;

                    case 2:
                        RetrieveObject(chaosArray);
                        break;

                    case 9:
                        Console.WriteLine("OK, i hope you found this useful. Goodbye. ");
                        break;

                    default:
                        Console.WriteLine("I am sorry, but that is not a valid choice. ");
                        break;
                }
            }
        }

        static void AddObject(ChaosArray chaos)
        {
            Console.WriteLine("What would you like to add? ");
            chaos.InsertObject(Console.ReadLine());
        }

        static void RetrieveObject(ChaosArray chaos)
        {
            object retrieved = chaos.RetrieveObject();
            if (retrieved != null)
            {
                Console.WriteLine("You fetched object: " + retrieved);
            }
        }

        static int ValidateNumberInput()
        {
            int temp;
            bool isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            while (!isANumber)
            {
                Console.WriteLine("I am sorry, that is not a valid number. Could you please enter a valid number (no decimals)");
                isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            }
            return temp;
        }
    }
}
