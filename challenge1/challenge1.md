# Challenge 1: Chaos array
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/challenge1

# Assignment text
• Create a custom collection - ChaosArray
	- It should be able to store any type
	- There should only be one way to insert an item in the collection – which is inserted randomly
	- There should only be one way to retrieve an item in the collection – which returns a random item
	- Use a custom Exception to handle instances where an item is being inserted into an unavailable space and when an attempt to retrieve an item that is not there 