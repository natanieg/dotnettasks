﻿using System;

namespace task12
{
    class PrintBoard
    {
        private char boardLetter;
        private char queenLetter;
        private ConsoleColor originalQueenColor = ConsoleColor.Red;

        /// <summary>
        /// Construct a board printer using default values.
        /// </summary>
        public PrintBoard()
        {
            boardLetter = 'x';
            queenLetter = 'Q';
        }
        /// <summary>
        /// Construct a board printer using your own values for the board and queen.
        /// </summary>
        /// <param name="board">The letter to represent the board (empty spots).</param>
        /// <param name="queen">The letter to represent the queen.</param>
        public PrintBoard(char board, char queen)
        {
            boardLetter = board;
            queenLetter = queen;
        }

        /// <summary>
        /// Prints just the board with the location of the queens in relation to each other.
        /// </summary>
        /// <param name="pieces">The board pieces.</param>
        /// <param name="originalQueen">The original queen placed by the user.</param>
        public void SimplePrint(int[,] pieces, int[,] originalQueen)
        {
            char[,] board = MakeBoard();
            board = PlaceQueens(board, pieces);
            PrintToConsole(board, 0, originalQueen);
        }

        /// <summary>
        /// Method to print a board with a boarder.
        /// </summary>
        /// <param name="pieces">Location of all of the queens.</param>
        /// <param name="originalQueen">The original queen placed by the user.</param>
        public void FancyPrint(int[,] pieces, int[,] originalQueen)
        {
            char[,] board = MakeBoard();
            board = PlaceQueens(board, pieces);
            board = AddBorder(board);
            PrintToConsole(board, 3, originalQueen);
        }

        /// <summary>
        /// Creates the chess board with the size of 8 x 8.
        /// </summary>
        /// <returns>A char array filled with the boardLetter.</returns>
        private char[,] MakeBoard()
        {
            char[,] board = new char[8, 8];

            // Drawing a board
            for (int i = 0; i < 8; i++)
            {
                for (int n = 0; n < 8; n++)
                {
                    board[i, n] = boardLetter;
                }
            }

            return board;
        }

        /// <summary>
        /// Places the queens into the given char[,] board with the queenLetter.
        /// </summary>
        /// <param name="board">The char[,] which is the board.</param>
        /// <param name="queenPlacements">An int [,] with the x and y coordinates of the queens.</param>
        /// <returns>A char array with the queen placements added on to it.</returns>
        private char[,] PlaceQueens(char[,] board, int[,] queenPlacements)
        {
            // Placing the queens onto the board
            for (int height = 0; height < 8; height++)
            {
                for (int width = 0; width < 8; width++)
                {
                    for (int i = 0; i < 8; i++)
                    {
                        if (queenPlacements[i, 0] == width && queenPlacements[i, 1] == height)
                        {
                            board[width, height] = queenLetter;
                        }
                    }
                }
            }
            return board;
        }

        /// <summary>
        /// Adds a boarder to the board sent in to match a chess board.
        /// </summary>
        /// <param name="board">The 8x8 chess board to add a border to.</param>
        /// <returns></returns>
        private char[,] AddBorder(char[,] board)
        {
            // New border and counters for the method for later.
            char[,] boardWithBorder = new char[11, 11];
            int numberIncrementer = 1;
            int letterIncrementer = 0;
            char[] letters = new char[]
            {
                'a',
                'b',
                'c',
                'd',
                'e',
                'f',
                'g',
                'h'
            };

            // Iterating through the board to print, including the original board and the border itself.
            for (int height = 0; height < 11; height++)
            {
                for (int width = 0; width < 11; width++)
                {
                    // Corners for the board
                    if (width == 1 && height == 1 || width == 10 && height == 1 ||
                        width == 1 && height == 10 || width == 10 && height == 10)
                    {
                        boardWithBorder[width, height] = '#';
                    }
                    // Vertical lines
                    else if ((width == 1 && height > 1) && (width == 1 && height < 10) ||
                            (width == 10 && height > 1) && (width == 10 && height < 10))
                    {
                        boardWithBorder[width, height] = '|';
                    }
                    // Horizontal lines
                    else if ((width > 1 && height == 1) && (width < 10 && height == 1) ||
                            (width > 1 && height == 10) && (width < 10 && height == 10))
                    {
                        boardWithBorder[width, height] = '-';
                    }
                    // Vertical numbers
                    else if ((width == 0 && height > 1) && (width == 0 && height < 10))
                    {
                        // 1 to 8
                        boardWithBorder[width, height] = char.Parse("" + numberIncrementer);
                        numberIncrementer++;
                    }
                    // Horizontal letters
                    else if ((width > 1 && height == 0) && (width < 10 && height == 0))
                    {
                        // a to h
                        boardWithBorder[width, height] = letters[letterIncrementer];
                        letterIncrementer++;
                    }
                    // And now, the actual chess board itself
                    else if (width > 1 && height > 1)
                    {
                        boardWithBorder[width, height] = board[width - 2, height - 2];
                    }
                }
            }

            return boardWithBorder;
        }

        /// <summary>
        /// Printing the selected board into console.
        /// </summary>
        /// <param name="board">The char 2d array containing the playing board to print</param>
        /// <param name="boarderSize">An int to compensate for any border, espesially from FancyBoard.</param>
        /// <param name="originalQueen">The X and Y location of the original piece the user placed.</param>
        private void PrintToConsole(char[,] board, int boarderSize, int[,] originalQueen)
        {
            // Compensating for the board being pushed slightly up into the corner.
            int originX = originalQueen[0, 0] + boarderSize - 1;
            int originY = originalQueen[0, 1] + boarderSize - 1;

            // Flipping board during the print to visualize it better, with a1 being the lower left hand corner
            for (int height = 7 + boarderSize; height >= 0; height--)
            {
                for (int width = 0; width < 8 + boarderSize; width++)
                {
                    // Painting the original piece we put down with our selected color to differenciate it.
                    if (width == originX && height == originY)
                    {
                        Console.ForegroundColor = originalQueenColor;
                    }
                    Console.Write(board[width, height]);
                    // Returning to default color for the console.
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.WriteLine();
            }
        }
    }
}
