﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task12
{
    class CheckForSolution
    {
        static int numberOfQueens = 8;
        static int boardSize = 8;
        static int queenPositionX;
        static int queenPositionY;
        int[,] board = new int[numberOfQueens, numberOfQueens];

        public CheckForSolution()
        {

        }

        public int[,] CheckForSolutionWithPosition(string position)
        {
            if (position.Length != 2)
            {
                Console.WriteLine("I am sorry, but " + position + " is not a valid position on a chessboard. " + position.Length);
                return null;
            }
            else
            {
                queenPositionX = char.ToUpper(position[0]) - 65;
                queenPositionY = (int)char.GetNumericValue(position[1]) - 1;
                Console.WriteLine(position + " is a valid input. X: " + queenPositionX + " - Y: " + queenPositionY);

                board[queenPositionY, queenPositionX] = 1;

                for (int i = 0; i < numberOfQueens; i++)
                {
                    for (int j = 0; j < numberOfQueens; j++)
                    {
                        Console.Write(board[i, j]);
                    }
                    Console.Write("\n");
                }
                Console.WriteLine("\n\n\n");

                if (!BoardSolver(board, queenPositionY))
                {
                    Console.WriteLine("Solution not found. \n ");
                    for (int i = 0; i < numberOfQueens; i++)
                    {
                        for (int j = 0; j < numberOfQueens; j++)
                        {
                            Console.Write(board[i, j]);
                        }
                        Console.Write("\n");
                    }
                    return null;
                }
                else
                {
                    return board;
                }
            }
        }

        static Boolean PlaceQueen(int[,] board, int x, int y)
        {
            /*for (int i = 0; i < y; i++)
            {
                if (board[i, y] == x) return true; // Same Row
                if ((board[i, y] + y - i == x) || (board[i, y] - y + i == x))
                    return false;
            }
            return true;*/
            int i, j;

            /* Check this row and column*/
            for (i = 0; i <= boardSize; i++)
            {
                if (board[((i + queenPositionY) % (boardSize - 1)), x] == 1)
                {
                    return false;
                }
                if (board[y, ((i + queenPositionX) % (boardSize - 1))] == 1)
                {
                    Console.WriteLine("X: " + (i + queenPositionX % boardSize) + " - Y: " + y + "---" + board[y, (i + queenPositionX % boardSize)]);
                    return false;
                }
            }

            /* Check upper diagonal on left side */
            for (i = y, j = x; i >= 0 && j >= 0; i--, j--)
                if (board[i, j] == 1)
                    return false;

            /* Check lower diagonal on left side */
            for (i = y, j = x; j >= 0 && i < boardSize; i++, j--)
                if (board[i, j] == 1)
                    return false;

            /* Check upper diagonal on right side */
            for (i = y, j = x; i <= 0 && j <= 0; i++, j++)
                if (board[i, j] == 1)
                    return false;

            /* Check lower diagonal on right side */
            for (i = y, j = x; j <= 0 && i > boardSize; i--, j++)
                if (board[i, j] == 1)
                    return false;

            /*

            //  Check if there are any queens on diagonal lines
            for (i = x, j = y; i >= 0 && j >= 0; i--, j--)
            {
                if (board[i, j] == 1) return false;
            }

            //  Check if there are any queens on diagonal lines
            for (i = x, j = y; j >= 0 && i < numberOfQueens; i++, j--)
            {
                if (board[i, j] == 1) return false;
            }*/
            return true;
        }

        Boolean BoardSolver(int[,] board, int y)
        {
            if (y >= boardSize)
            {
                for (int i = 0; i < boardSize; i++)
                {
                    for (int j = 0; j < boardSize; j++)
                    {
                        Console.Write(board[i, j]);
                    }
                    Console.Write("\n");
                }

                return true;
            }
            for (int i = 1; i < numberOfQueens - 1; i++)
            {
                if (PlaceQueen(board, i, y))
                {
                    board[y, i] = 1;
                    if (BoardSolver(board, (y + queenPositionY) % (boardSize - 1) + 1))
                    {
                        return true;
                    }
                    board[y, i] = 0;
                }
            }
            return false;
        }
    }
}
