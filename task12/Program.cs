﻿using System;

namespace task12
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            * Group assigment by:
            * Nataniel Gåsøy
            * Øyvind Mathisen
            */

            /// Mathematical solution not functioning
            //CheckForSolution solution = new CheckForSolution();
            //solution.CheckForSolutionWithPosition("A4");


            PrintBoard printer = new PrintBoard();
            KnownSolutionBuilder builder = new KnownSolutionBuilder();

            int[,] selectedPosition = GetUserInput();
            int[,] solutionFound = LocateMatchingSolution(selectedPosition, builder);

            if (solutionFound == null)
            {
                Console.WriteLine("No solution found");
            }
            else
            {
                // printer.SimplePrint(queenPositions);
                printer.FancyPrint(solutionFound, selectedPosition);
            }

            // Terminate program
            Console.WriteLine();
            Console.WriteLine("Press any non-function key to terminate the program.");
            Console.ReadKey();
        }

        static int[,] GetUserInput()
        {
            bool validInput = false;
            int[,] queenPosition = new int[2, 2];
            Console.WriteLine("Input your coordinates. Letter followed by number (row, then column).\nEnd your input with enter.\n");

            // Loops until valid input has been used.
            while (!validInput)
            {
                string input = Console.ReadLine();
                // Checking for input length.
                if (input.Length != 2)
                {
                    Console.WriteLine("The input: '" + input + "' is not a valid position on the chess board.\nPlease try again.\n");
                    continue;
                }

                // If input is valid, checking if the range is within the chess board
                int xInput = char.ToUpper(input[0]) - 65;
                int yInput = (int)char.GetNumericValue(input[1]) - 1;

                // If input is not on the board, present error and return to input phase.
                if (xInput < 0 || xInput > 7 || yInput < 0 || yInput > 7)
                {
                    Console.WriteLine("The input: '" + input + "' is not a valid position on the chess board.\nPlease try again.\n");
                    continue;
                }

                // Input accepted.
                queenPosition[0, 0] = xInput;
                queenPosition[0, 1] = yInput;
                break;
            }
            return queenPosition;
        }

        static int[,] LocateMatchingSolution(int[,] selectedPosition, KnownSolutionBuilder solutionBuilder)
        {
            // Build the solutions and check up against the position the user selected.
            foreach (int[,] solution in solutionBuilder.GetValidSolutions())
            {
                for (int i = 0; i < 8; i++)
                {
                    if (solution[i, 0] == selectedPosition[0, 0] && solution[i, 1] == selectedPosition[0, 1])
                    {
                        // Match found! Returning this.
                        return solution;
                    }
                }
            }

            // No solution found.
            return null;
        }
    }
}
