# Assignment 12: 8-queen problem
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task12

# Assignment text
• Write a program that takes in a co-ordinate on the board
• It must then tell me if it is possible to complete the 8-queens problem with a queen at the chosen location
• If it is possible it must print a board (in console) that shows one such possible solution