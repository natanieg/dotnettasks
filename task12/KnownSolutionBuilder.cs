﻿using System;
using System.Collections.Generic;

namespace task12
{
    class KnownSolutionBuilder
    {
        private List<int[,]> solutions = new List<int[,]>();

        public KnownSolutionBuilder()
        {
            solutions = MakeDistinctSolutions();
            solutions = MakeVariationSolutions(solutions);
        }

        public List<int[,]> GetValidSolutions()
        {
            return solutions;
        }

        private List<int[,]> MakeDistinctSolutions()
        {
            List<int[,]> collection = new List<int[,]>();

            collection.Add(new int[,]
            {
                {0, 1},
                {1, 3},
                {2, 5},
                {3, 7},
                {4, 2},
                {5, 0},
                {6, 6},
                {7, 4}
            });

            collection.Add(new int[,]
            {
                {0, 0},
                {1, 6},
                {2, 3},
                {3, 5},
                {4, 7},
                {5, 1},
                {6, 4},
                {7, 2}
            });

            collection.Add(new int[,]
            {
                {0, 0},
                {1, 6},
                {2, 4},
                {3, 7},
                {4, 1},
                {5, 3},
                {6, 5},
                {7, 2}
            });

            collection.Add(new int[,]
            {
                {0, 3},
                {1, 0},
                {2, 4},
                {3, 7},
                {4, 1},
                {5, 6},
                {6, 2},
                {7, 5}
            });

            collection.Add(new int[,]
            {
                {0, 4},
                {1, 0},
                {2, 7},
                {3, 3},
                {4, 1},
                {5, 6},
                {6, 2},
                {7, 5}
            });

            collection.Add(new int[,]
            {
                {0, 2},
                {1, 0},
                {2, 6},
                {3, 4},
                {4, 7},
                {5, 1},
                {6, 3},
                {7, 5}
            });

            collection.Add(new int[,]
            {
                {0, 4},
                {1, 0},
                {2, 3},
                {3, 5},
                {4, 7},
                {5, 1},
                {6, 6},
                {7, 2}
            });

            collection.Add(new int[,]
            {
                {0, 6},
                {1, 0},
                {2, 2},
                {3, 7},
                {4, 5},
                {5, 3},
                {6, 1},
                {7, 4}
            });

            collection.Add(new int[,]
            {
                {0, 4},
                {1, 0},
                {2, 7},
                {3, 5},
                {4, 2},
                {5, 6},
                {6, 1},
                {7, 3}
            });

            collection.Add(new int[,]
            {
                {0, 4},
                {1, 6},
                {2, 0},
                {3, 3},
                {4, 1},
                {5, 7},
                {6, 5},
                {7, 2}
            });

            collection.Add(new int[,]
            {
                {0, 5},
                {1, 2},
                {2, 0},
                {3, 7},
                {4, 3},
                {5, 1},
                {6, 6},
                {7, 4}
            });

            collection.Add(new int[,]
            {
                {0, 4},
                {1, 2},
                {2, 0},
                {3, 6},
                {4, 1},
                {5, 7},
                {6, 5},
                {7, 3}
            });

            return collection;
        }

        private List<int[,]> MakeVariationSolutions(List<int[,]> collection)
        {
            List<int[,]> newSolutions = new List<int[,]>();

            foreach (int[,] distinctSolution in collection)
            {
                int[,] variantSolution = distinctSolution;

                for (int i = 0; i < 4; i++)
                {
                    newSolutions.Add(variantSolution); // Base solution
                    newSolutions.Add(ReflectSolution(variantSolution)); // Reflected solution, copy
                    variantSolution = RotateSolution(variantSolution); // Rotating the solution for the next pass.
                }
            }
            return newSolutions;
        }

        private int[,] RotateSolution(int[,] solution)
        {
            int[,] rotatedSolution = new int[8, 2];

            for (int i = 0; i < 8; i++)
            {
                double x = solution[i, 0];
                double y = solution[i, 1];
                double center = 3.5;

                x -= center;
                y -= center;

                // 90 degrees in radian = PI / 2, also known as theta.
                // cos(90) = 0, sin(90) = 1.
                // x * cos(theta) - y * sin(theta).
                double newX = x * 0 - y * 1;
                // x * sin(theta) + y * cos(theta)
                double newY = x * 1 + y * 0;

                newX += center;
                newY += center;

                rotatedSolution[i, 0] = (int)newX;
                rotatedSolution[i, 1] = (int)newY;
            }
            return rotatedSolution;
        }

        // Only counting horizontal reflections, not diagonal or vertical ones.
        private int[,] ReflectSolution(int[,] solution)
        {
            // Creating a new object to avoid changing the already existing object on the heap / list.
            int[,] reflectedSolution = new int[8, 2];

            for (int i = 0; i < 8; i++)
            {
                reflectedSolution[i, 0] = Math.Abs(solution[i, 0] - 7);
                reflectedSolution[i, 1] = solution[i, 1];
            }
            return reflectedSolution;
        }
    }
}
