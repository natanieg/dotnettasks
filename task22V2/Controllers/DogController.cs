﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using task22.Models;
using Microsoft.AspNetCore.Http;

namespace task22.Controllers
{
    public class DogController : Controller
    {
        public IActionResult Index()
        {
            PictureModel picture = new PictureModel();
            Picture pic = new Picture();

            try
            {
                pic = picture.GetDog();
            }
            catch (Exception exp)
            {
                return View(exp.Message);
            }

            return View("Dog", pic);
        }
    }
}