﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace task22.Models
{
    public class PictureModel
    {
        static GameState game;

        public PictureModel()
        {
            game = new GameState();
        }
        public Picture GetDog()
        {
            game.currentAnswer = "Dog";
            return GetAnimalPicture("https://random.dog/woof.json", AnimalType.Dog);
        }

        public Picture GetCat()
        {
            game.currentAnswer = "Cat";
            return GetAnimalPicture("https://aws.random.cat/meow", AnimalType.Cat);
        }

        public Picture GetRandom()
        {
            Random rand = new Random();
            int current = rand.Next(0, 2);  //0 = dog, 1 = cat

            switch (current)
            {
                case 0:
                    return GetDog();

                case 1:
                    return GetCat();

                default:
                    game.currentAnswer = "None";
                    return GetStockPhoto();
            }
        }

        public Picture GetStockPhoto()
        {
            Picture picObj = new Picture();
            picObj.source = @"https://picsum.photos/536/354";
            return picObj;
        }

        public Picture GetAnimalPicture(string URL, AnimalType type)
        {
            string json;

            Picture pic = new Picture();
            do
            {
                using (WebClient wc = new WebClient())
                {
                    try
                    {
                        json = wc.DownloadString(URL);
                    }
                    catch (WebException exp)
                    {
                        throw new Exception(exp.Message);
                    }
                }
                pic = JsonConvert.DeserializeObject<Picture>(json);
                if (type == AnimalType.Cat)
                {
                    pic.source = pic.file;
                    pic.currentAnimal = AnimalType.Cat;
                }
                else if (type == AnimalType.Dog)
                {
                    pic.source = pic.url;
                    pic.currentAnimal = AnimalType.Dog;
                }
            } while (!ValidDisplayType(pic.source));

            return pic;
        }

        private bool ValidDisplayType(string path)
        {
            if (Path.GetExtension(path).Equals(".mp4") ||
                Path.GetExtension(path).Equals(".webm"))
            {
                return false;
            }
            return true;
        }
    }
}
