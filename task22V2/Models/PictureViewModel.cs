﻿namespace task22.Models
{
    public class Picture
    {
        public string source { get; set; }
        public string url { get; set; }
        public string file { get; set; }
        public AnimalType currentAnimal { get; set; }
    }

    public enum AnimalType
    {
        Cat,
        Dog,
    }
}
