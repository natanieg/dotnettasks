﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task22.Models
{
    public class GameStateModel
    {

        public bool CheckAnswer(GameState game, string answer)
        {
            if (answer.Equals(game.currentAnswer))
            {
                game.correctAnswerCount++;
                return true;
            }
            return false;
        }
    }


}
