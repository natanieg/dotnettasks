﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task11
{
    interface ISavingsCard
    {
        int savingAmount { get; set; }
    }
}
