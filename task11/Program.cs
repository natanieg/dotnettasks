﻿using System;

namespace task11
{
    class Program : Card, ICash
    {
        static void Main(string[] args)
        {
            int cost = 120;

            Console.WriteLine("Hello, that will be " + cost + ". How would you like to pay today?" +
                "\n1: Credit card. " +
                "\n2: Savings card. " +
                "\n3: Cash. ");
            int choice = ValidateNumberInput();

            switch (choice)
            {
                case 1:
                    PaymentCreditCard(cost);
                    break;

                case 2:
                    PaymentSavingsCard(cost);
                    break;

                case 3:
                    ICash payCash = new Program();
                    payCash.PayWithCash(cost);
                    break;

                default:
                    Console.WriteLine("I am sorry, but '" + choice + "' is not an accepted payment method");
                    break;
            }
        }
        static void PaymentSavingsCard(int cost)
        {
            Console.WriteLine("You got it, processing payment with savings card. ");
            Card savingsCard = new Card();
            savingsCard.savingAmount = 2000;
            savingsCard.savingAmount -= cost;
            Console.WriteLine("OK, your savings amount has been reduced. Your new savings amount is: " + savingsCard.savingAmount);

        }

        static void PaymentCreditCard(int cost)
        {
            Console.WriteLine("You got it, processing payment with credit card. ");
            Card creditCard = new Card();

            creditCard.creditLimit = 2000;
            creditCard.creditLimit -= cost;
            Console.WriteLine("OK, your credit limit has been reduced. Your new credit limit is: " + creditCard.creditLimit);


        }

        public void PayWithCash(int costAmmount)
        {
            Console.WriteLine("You got it, how much are you paying with? ");
            int paid = ValidateNumberInput();
            Console.WriteLine("Very good, your change is: " + (paid - costAmmount));
        }

        static int ValidateNumberInput()
        {
            int temp;
            bool isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            while (!isANumber)
            {
                Console.WriteLine("I am sorry, that is not a valid number. Could you please enter a valid number (no decimals)");
                isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            }
            return temp;
        }
    }
}
