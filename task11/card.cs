﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task11
{
    class Card : ICreditCard, ISavingsCard
    {
        int cardNumber { get; set; }
        string cardNameHolder { get; set; }


        public int savingAmount { get; set; }
        public int creditLimit { get; set; }

    }
}
