# Assignment 11: Payment System
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task11

# Assignment text
• Consider a payment system
	- Customers may use cash or card
	- A card may be a savings card or a credit card
	- Cash will probably require change
• Design a system that allows for these options:
	- Use an interface or inheritance or both?
	- Implement your solution in a simple console application