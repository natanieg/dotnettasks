﻿using System;

namespace task8
{
    class Task8
    {
        static void Main(string[] args)
        {
            int choice = 0;
            while (choice != 9)
            {
                Console.Write("\n\nWhat program would you like to run today? " +
                    "\n1: BMI calculator. " +
                    "\n2: Square. " +
                    "\n3: Nested square. " +
                    "\n\n9: Quit" +
                    "\n\nInput: ");
                choice = ValidateNumberInput();
                switch (choice)
                {
                    case 1:
                        BMICalculator();
                        break;

                    case 2:
                        Square();
                        break;

                    case 3:
                        NestedSquare();
                        break;

                    case 9:
                        Console.WriteLine("OK, shutting down now. Have a nice day. ");
                        break;

                    default:
                        Console.WriteLine("I am sorry, but that is not a valid option");
                        break;
                }
            }
        }

        static void BMICalculator()
        {
            float height;
            float weight;
            float BMI;

            Console.WriteLine("Hello there, it is time to check your BMI!");
            Console.Write("For starters, what is your height (in cm)? ");
            height = ValidateNumberInput();

            Console.Write("OK, that is good, your height is " + height + ". And what is your weight (in kg)? ");
            weight = ValidateNumberInput();
            Console.WriteLine("Your weight is " + weight + ".");

            BMI = weight / MathF.Pow((height / 100), 2);
            switch (BMI)
            {
                case float n when (n < 18.5):
                    Console.WriteLine("Your BMI of " + BMI.ToString("0.00") + " is considered to be: underweight");
                    break;

                case float n when (n >= 18.5 && n < 25):
                    Console.WriteLine("Your BMI of " + BMI.ToString("0.00") + " is considered to be: normal weight");
                    break;

                case float n when (n >= 25 && n < 30):
                    Console.WriteLine("Your BMI of " + BMI.ToString("0.00") + " is considered to be: overweight");
                    break;

                case float n when (n >= 30):
                    Console.WriteLine("Your BMI of " + BMI.ToString("0.00") + " is considered to be: obese");
                    break;

                default:
                    Console.WriteLine("There was something wrong with the BMI. It is registered as: " + BMI.ToString("0.00"));
                    break;
            }
        }

        static void Square()
        {
            int noOfColumns;
            int noOfRows;

            Console.WriteLine("Hello there, today we are making a square. First, how high should the cube be?");
            noOfColumns = ValidateNumberInput();

            Console.WriteLine("OK good, it should be " + noOfColumns + " tall, and how wide should it be?");
            noOfRows = ValidateNumberInput();

            int[,] square = new int[noOfColumns, noOfRows];
            Console.WriteLine("Good, and " + noOfRows + " wide. Now, let's see, your square should look something like this: \n");

            for (int i = 0; i < noOfColumns; i++)
            {
                for (int j = 0; j < noOfRows; j++)
                {
                    if (i == 0 || i == (noOfColumns - 1) || j == 0 || j == (noOfRows - 1))
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.Write('\n');
            }
        }


        static void NestedSquare()
        {
            int noOfColumns;
            int noOfRows;
            int borderSize = 1;

            #region GetInput
            Console.WriteLine("Hello there, today we are making a square. First, how high should the cube be? ");
            noOfColumns = ValidateNumberInput();

            Console.WriteLine("OK good, it should be " + noOfColumns + " tall, and how wide should it be? ");
            noOfRows = ValidateNumberInput();

            Console.WriteLine("How large should the border between the outer and inner square be? ");
            borderSize = ValidateNumberInput();
            #endregion

            int[,] square = new int[noOfColumns, noOfRows];
            Console.WriteLine("Good, and " + noOfRows + " wide. Now, let's see, your square should look something like this: \n");

            for (int i = 0; i < noOfColumns; i++)
            {
                for (int j = 0; j < noOfRows; j++)
                {
                    if (i == 0 || i == (noOfColumns - 1) || j == 0 || j == (noOfRows - 1)                                   // Outer square
                                                                                                                            /*
                                                                                                                            || (i == 1 + borderSize && j >= 1 + borderSize && j < noOfRows - borderSize - 1)                    // Inner square, top row
                                                                                                                            || (j == 1 + borderSize && i >= 1 + borderSize && i < noOfColumns - borderSize - 1)                 // Inner square, bottom row
                                                                                                                            || (i == noOfColumns - borderSize - 2 && j >= 1 + borderSize && j < noOfRows - borderSize - 1)      // Inner square, left column                                                                                  // Inner square, left column
                                                                                                                            || (j == noOfRows - borderSize - 2 && i >= 1 + borderSize && i < noOfColumns - borderSize - 1))     // Inner square, right column
                                                                                                                            */
                        || ((i == 1 + borderSize || i == noOfRows - borderSize - 2) && (j > borderSize && j < noOfRows - borderSize - 1))           // Horizontal lines of inner square
                        || ((j == 1 + borderSize || j == noOfColumns - borderSize - 2) && (i > borderSize && i < noOfColumns - borderSize - 1)))    // Vertical lines of inner square
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write("|");
                    }
                }
                Console.Write('\n');
            }
        }

        static int ValidateNumberInput()
        {
            int temp;
            bool isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            while (!isANumber)
            {
                Console.WriteLine("I am sorry, that is not a valid number. Could you please enter a valid number (no decimals)");
                isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            }
            return temp;
        }
    }
}
