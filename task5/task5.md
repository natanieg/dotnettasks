# Assignment 5: Name search
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task5

# Assignment text
• Write a program which stores a sample of 5 contact
names
• First and Last name
• Then allows the user to search the list by name and
determine if the name is in the list
• Partial matches should work
• Display all possible matches