﻿using System;
using System.Collections.Generic;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {
            string searchName;
            List<string> contacts = new List<string>();
            List<string> contactMatches = new List<string>();
            PopulateContactList(contacts);

            Console.WriteLine("Hello there, who would you like to search for today?: ");
            searchName = Console.ReadLine();

            foreach (string contact in contacts)
            {
                if (contact.ToLower().Contains(searchName.ToLower()))
                {
                    contactMatches.Add(contact);
                }
            }

            if (contactMatches != null && contactMatches.Count > 0)
            {
                Console.WriteLine("The current contacts matching your search query: ");
                foreach (string contact in contactMatches)
                {
                    Console.WriteLine("      - " + contact);
                }
            }
            else
            {
                Console.WriteLine("I am sorry, but there does not seem to be any contacts by that name");
            }
        }

        static List<string> PopulateContactList(List<string> list)
        {
            list.Add("John Wick");
            list.Add("John Adams");
            list.Add("Alexander Hamilton");
            list.Add("Thomas Jefferson");
            list.Add("Hercules Mulligan");
            list.Add("Marquis De La Fayette");
            list.Add("Christopher Robin");
            return list;
        }
    }
}
