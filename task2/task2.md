# Assignment 2: Hello world with input
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task1

# Assignment text
• Write a console application
• The program must:
     - Allow the user to input their name: example “XYZ”
     - Print “Hello XYZ, your name is 3 characters long and starts with a X.” 