﻿using System;

namespace task2
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;
            Console.WriteLine("Hello mr. Lecturer person! What is your name?");
            name = Console.ReadLine();
            Console.WriteLine("Hello, " + name + "! \n" +
                "Did you know that your name is " + name.Length + " characters long? \n" +
                "Or that it starts with an " + name[0] + "? \n" +
                "Fascinating. ");
        }
    }
}
