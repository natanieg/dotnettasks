﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using RPGCharacterGenerator;
using System.IO;
using System.Diagnostics;
using System.Data.SQLite;

namespace task18
{
    public partial class FormCharacterCreation : Form
    {
        Character character;
        List<string> subclasses;
        List<string> characterClasses = new List<string>();
        List<Type> characterClassType = new List<Type>();
        List<Type> characterClassSubtype;
        SQLiteConnection conn;// = ConnectToDatabase();

        public FormCharacterCreation()
        {
            InitializeComponent();
            InitializeClassDropdown();
            PopulateExistingCharacterList();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void InitializeClassDropdown()
        {
            var classTypes = Assembly
                .GetAssembly(typeof(Character))
                .GetTypes()
                .Where(t => t.BaseType == (typeof(Character)));

            foreach (Type characterClass in classTypes)
            {
                characterClasses.Add(characterClass.Name);
                characterClassType.Add(characterClass);
            }

            this.dropdownClass.DataSource = characterClasses;
        }

        private void DropdownClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = dropdownClass.SelectedIndex;
            Type parent = characterClassType[index];

            var subclassTypes = Assembly
                .GetAssembly(typeof(Character))
                .GetTypes()
                .Where(t => t.BaseType == parent);

            subclasses = new List<string>();
            characterClassSubtype = new List<Type>();
            foreach (Type subclass in subclassTypes)
            {
                subclasses.Add(subclass.Name);
                characterClassSubtype.Add(subclass);
            }

            this.dropdownSubclass.DataSource = subclasses;
        }
        private void ButtonCreateCharacter_Click(object sender, EventArgs e)
        {
            //Type characterClass = characterClassSubtype[dropdownSubclass.SelectedIndex];
            //
            //character = (Character)Activator.CreateInstance(characterClass,
            //    (int)numHP.Value, (int)numMP.Value, tbName.Text, (int)numAC.Value, tbAttack.Text, (int)numMovement.Value);
            //SaveCharacterToFile(character);
            conn = ConnectToDatabase();


            //--------- TODO Insert this into a subquery
            int classID = 0;
            SQLiteDataReader sqliteDataClass;
            SQLiteCommand sqliteCmdClass = conn.CreateCommand();
            sqliteCmdClass.CommandText = "" +
                "SELECT ClassID " +
                "FROM Class " +
                "WHERE Class.Name= '" + dropdownSubclass.Text + "';";
            sqliteDataClass = sqliteCmdClass.ExecuteReader();

            while (sqliteDataClass.Read())
            {
                classID = sqliteDataClass.GetInt32(0);
            }
            // -------------------------------------------

            SQLiteCommand sqliteCmdCreate = conn.CreateCommand();
            sqliteCmdCreate.CommandText = "" +
                "INSERT INTO Character (Name, HP, MP, AC, ClassID ) " +
                "VALUES ('" + tbName.Text + "', " + (int)numHP.Value + ", " + (int)numMP.Value + ", " + (int)numAC.Value + ", " + classID + ");";
            sqliteCmdCreate.ExecuteNonQuery();
            DisplayCharacter();
            conn.Close();

        }

        private void DisplayCharacter(/*Character character*/)
        {
            listViewCharacter.Clear();
            listViewCharacter.View = View.Tile;
            listViewCharacter.Columns.Add("File type", 250, HorizontalAlignment.Left);

            conn = ConnectToDatabase();
            SQLiteDataReader sqliteDataBaseInfo;
            SQLiteCommand sqliteCmdBaseInfo = conn.CreateCommand();
            sqliteCmdBaseInfo.CommandText = "" +
                "SELECT Character.Name, Class.Name, Character.HP, Character.MP, Character.AC " +
                "FROM Character " +
                "INNER JOIN Class " +
                "ON Character.ClassID = Class.ClassID " +
                "WHERE Character.Name= '" + dropdownExistingCharacters.SelectedItem.ToString() + "';";
            sqliteDataBaseInfo = sqliteCmdBaseInfo.ExecuteReader();

            while (sqliteDataBaseInfo.Read())
            {
                listViewCharacter.Items.Add("Name: " + sqliteDataBaseInfo.GetString(0).ToString());
                listViewCharacter.Items.Add("Class: " + sqliteDataBaseInfo.GetString(1));
                listViewCharacter.Items.Add("HP: " + sqliteDataBaseInfo.GetInt32(2).ToString());
                listViewCharacter.Items.Add("MP: " + sqliteDataBaseInfo.GetInt32(3).ToString());
                listViewCharacter.Items.Add("AC: " + sqliteDataBaseInfo.GetInt32(4).ToString());

                tbName.Text = (sqliteDataBaseInfo.GetString(0).ToString());
                numHP.Value = (sqliteDataBaseInfo.GetInt32(2));
                numMP.Value = (sqliteDataBaseInfo.GetInt32(3));
                numAC.Value = (sqliteDataBaseInfo.GetInt32(4));

                dropdownSubclass.Text = (sqliteDataBaseInfo.GetString(1));

                bool correctClass = false;
                int indexClass = 0;
                int indexSubclass = 0;
                while (!correctClass)
                {
                    Type parent;
                    if (indexClass < 3)
                    {
                        parent = characterClassType[indexClass];
                    }
                    else
                    {
                        parent = characterClassType[((int)indexClass / 3) - 1];
                    }

                    var subclassTypes = Assembly
                        .GetAssembly(typeof(Character))
                        .GetTypes()
                        .Where(t => t.BaseType == parent);

                    foreach (Type subclass in subclassTypes)
                    {
                        if (subclass.Name == sqliteDataBaseInfo.GetString(1))
                        {
                            correctClass = true;
                            MessageBox.Show(subclass.Name + " - " + parent.Name + indexSubclass + "-" + ((indexSubclass % 2)));
                            if (indexClass < 3)
                            {
                                dropdownClass.SelectedIndex = indexClass;
                            }
                            else
                            {
                                dropdownClass.SelectedIndex = (((int)indexClass / 3) - 1);
                            }
                            //TODO Fix this super hacky method
                            dropdownSubclass.SelectedIndex = 0;
                            if (dropdownSubclass.SelectedValue.ToString() != subclass.Name) dropdownSubclass.SelectedIndex = 1;
                        }
                    }
                    indexClass++;
                    indexSubclass++;
                }

            }

            SQLiteDataReader sqliteDataAction;
            SQLiteCommand sqliteCmdAttack = conn.CreateCommand();
            sqliteCmdAttack.CommandText =
                "SELECT Action.Name " +
                "FROM Character " +
                "INNER JOIN Class " +
                "ON Character.ClassID = Class.ClassID " +
                "INNER JOIN ClassAction " +
                "ON Class.ClassID = ClassAction.ClassID " +
                "INNER JOIN Action " +
                "ON Action.ActionID = ClassAction.ActionID " +
                "WHERE Character.Name= '" + dropdownExistingCharacters.SelectedItem.ToString() + "'" +
                "AND Action.Type = 'Attack';";
            sqliteDataAction = sqliteCmdAttack.ExecuteReader();
            int attackActions = 0;
            while (sqliteDataAction.Read())
            {
                listViewCharacter.Items.Add("Main attack: " + sqliteDataAction.GetString(attackActions));

                //tbAttack.Text = (sqliteDataAction.GetString(attackActions));
            }

            SQLiteDataReader sqliteDataMove;
            SQLiteCommand sqliteCmdMove = conn.CreateCommand();
            sqliteCmdMove.CommandText =
                "SELECT Action.Value " +
                "FROM Character " +
                "INNER JOIN Class " +
                "ON Character.ClassID = Class.ClassID " +
                "INNER JOIN ClassAction " +
                "ON Class.ClassID = ClassAction.ClassID " +
                "INNER JOIN Action " +
                "ON Action.ActionID = ClassAction.ActionID " +
                "WHERE Character.Name= '" + dropdownExistingCharacters.SelectedItem.ToString() + "'" +
                "AND Action.Type = 'Move';";
            sqliteDataMove = sqliteCmdMove.ExecuteReader();
            int moveAction = 0;
            while (sqliteDataMove.Read())
            {
                //MessageBox.Show(sqlite_datareader3.GetString(moveAction).ToString());
                listViewCharacter.Items.Add("Movement speed: " + sqliteDataMove.GetInt32(moveAction));

                //numMovement.Value = (sqliteDataMove.GetInt32(moveAction));
            }
            conn.Close();
        }

        public void SaveCharacterToFile(Character character)
        {

            const string filepath = @"..\..\..\savedCharacter\character.txt";
            using (var file = new StreamWriter(filepath))
            {
                file.WriteLine(character.HP);
                file.WriteLine(character.MP);
                file.WriteLine(character.Name);
                file.WriteLine(character.AC);
                file.WriteLine(character.Attack);
                file.WriteLine(character.Movement);

                file.WriteLine(character.GetClass());

                file.Close();

                Process.Start(filepath);
            }

        }
        static private SQLiteConnection ConnectToDatabase()
        {
            var pathDB = Path.Combine(Environment.CurrentDirectory, "task18.db");
            if (!File.Exists(pathDB)) throw new Exception();
            var connection_string = String.Format("Data Source={0};Version=3;New = True; Compress =True; ", pathDB);
            SQLiteConnection conn = new SQLiteConnection(connection_string);

            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return conn;
        }

        private void PopulateExistingCharacterList()
        {

            conn = ConnectToDatabase();
            List<string> existingCharacters = new List<string>();
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqliteCmd = conn.CreateCommand();
            sqliteCmd.CommandText = "SELECT Name FROM Character;";
            sqlite_datareader = sqliteCmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                existingCharacters.Add(sqlite_datareader.GetString(0).ToString());
            }
            dropdownExistingCharacters.DataSource = existingCharacters;
            conn.Close();
        }

        private void DropdownExistingCharacters_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayCharacter();
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            conn.Close();
        }

        private void ButtonUpdateCharacter_Click(object sender, EventArgs e)
        {
            if (tbName.Text == dropdownExistingCharacters.SelectedItem.ToString())
            {
                int classID = 0;
                conn = ConnectToDatabase();
                SQLiteDataReader sqliteDataClass;
                SQLiteCommand sqliteCmdClass = conn.CreateCommand();
                sqliteCmdClass.CommandText = "" +
                    "SELECT ClassID " +
                    "FROM Class " +
                    "WHERE name= '" + dropdownSubclass.SelectedItem.ToString() + "';";
                sqliteDataClass = sqliteCmdClass.ExecuteReader();

                while (sqliteDataClass.Read())
                {
                    classID = sqliteDataClass.GetInt32(0);
                }
                MessageBox.Show(classID.ToString());

                conn = ConnectToDatabase();
                SQLiteCommand sqliteCmdUpdateSelected = conn.CreateCommand();
                sqliteCmdUpdateSelected.CommandText =
                    "UPDATE Character " +
                    "SET HP = " + (int)numHP.Value + ", MP = " + (int)numMP.Value + ", AC = " + (int)numAC.Value + ", ClassID = " + classID + " " +
                    "WHERE Name= '" + dropdownExistingCharacters.SelectedItem.ToString() + "';";
                sqliteCmdUpdateSelected.ExecuteNonQuery();



                DisplayCharacter();
                conn.Close();
            }
            else
            {
                MessageBox.Show("Cannot give characters a new name. " + tbName.Text + " - " + dropdownExistingCharacters.SelectedItem.ToString());
            }
        }

        private void ButtonDeleteCharacter_Click(object sender, EventArgs e)
        {

            conn = ConnectToDatabase();
            SQLiteCommand sqliteCmdDeleteSelected = conn.CreateCommand();
            sqliteCmdDeleteSelected.CommandText = "" +
                "DELETE FROM Character " +
                "WHERE Character.Name= '" + dropdownExistingCharacters.SelectedItem.ToString() + "';";
            sqliteCmdDeleteSelected.ExecuteNonQuery();
            PopulateExistingCharacterList();
            DisplayCharacter();
            conn.Close();
        }

        private void ButtonClearCharacterList_Click(object sender, EventArgs e)
        {
            tbName.Text = "";
            dropdownSubclass.Text = "";
            numHP.Value = 0;
            numMP.Value = 0;
            numAC.Value = 0;
        }
    }
}
