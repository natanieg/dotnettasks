# Assignment 18: RPG DB with SQLite Database Connection Manager
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task18

# Assignment text
• Upgrade your RPG to use the SQLite Database Connection Manager instead of generation text based queries
• Upgrade your UI to allow for Deleting, Updating and Displaying characters from the database
• Practise using different commands with parameters to interact with your 'database' 
