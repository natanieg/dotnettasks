﻿using System;
using System.Collections.Generic;
using System.Text;
using task13.Interfaces;

namespace task13
{
    class Animal : IMove
    {
        public string animalName { get; set; }
        public List<Movement> locomotionList = new List<Movement>();

        public Animal()
        {

        }

        public Animal(string name)
        {
            animalName = name;
        }
    }

    enum Movement
    {
        Climb,
        Fly,
        Run,
        Swim,
        Walk,
    }

}
