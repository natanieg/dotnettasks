﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task13.Interfaces
{
    interface ISwim : IMove
    {
        void Swim();
    }
}
