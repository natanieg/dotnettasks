# Assignment 13: Animals and pets
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task13

# Assignment text
- An animal can be Herbivore, Carnivore or Omnivore
- Create Interfaces for movement types:
	• Walk, Run, Fly, Swim Climb
	• Each should write a string the the console
- The animal class should provide a means for “exposing” a list of movement types (Perhaps a list of enums?)
- Build a console application that that creates a collection of Animals
- Create a variety of animals (Min. 3)
- For each animal, randomly pick one of its movement types and display (run it).
- Example output for cat (walk, run, climb):
	> The cat can walk, run, and climb.
	> The cat runs.