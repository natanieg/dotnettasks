﻿using System;
using System.Collections.Generic;
using task13.Animals;
using task13.Interfaces;

namespace task13
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();

            animals.Add(new Cat("Felicia"));
            animals.Add(new Cow("Ferdinan"));
            animals.Add(new Fox("Balder"));

            foreach (Animal animal in animals)
            {
                string move = "This is a " + animal.GetType().Name.ToLower() + " named " + animal.animalName + ", who can ";
                int movementTypes = animal.locomotionList.Count;
                for (int i = 0; i < movementTypes; i++)
                {
                    move += animal.locomotionList[i].ToString().ToLower();
                    if (i < movementTypes - 2)
                    {
                        move += ", ";
                    }
                    else if (i == movementTypes - 2)
                    {
                        move += " and ";
                    }
                    else
                    {
                        move += ".";
                    }
                }
                Console.WriteLine(move);

                Random rand = new Random();
                string moveChoice = animal.locomotionList[rand.Next(movementTypes)].ToString();

                switch (moveChoice)
                {
                    case "Climb":
                        ((IClimb)animal).Climb();
                        break;

                    case "Fly":
                        ((IFly)animal).Fly();
                        break;

                    case "Run":
                        ((IRun)animal).Run();
                        break;

                    case "Swim":
                        ((ISwim)animal).Swim();
                        break;

                    case "Walk":
                        ((IWalk)animal).Walk();
                        break;

                    default:
                        throw new Exception("Movement type not recognized");

                }
            }
        }
    }
}
