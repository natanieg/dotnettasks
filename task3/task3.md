# Assignment 3: Make a square in the consol based on user input.
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task3

# Assignment text
Write a program which can be used to print a square of size chosen by the user at runtime.
The square can be made of any character that you choose. (# is probably a good choice)
The file must compile without errors. example: (after compilation)