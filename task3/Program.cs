﻿using System;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int noOfColumns;
            int noOfRows;

            Console.WriteLine("Hello there, today we are making a square. First, how high should the cube be?");
            noOfColumns = ValidateNumberInput();

            Console.WriteLine("OK good, it should be " + noOfColumns + " tall, and how wide should it be?");
            noOfRows = ValidateNumberInput();

            int[,] square = new int[noOfColumns, noOfRows];
            Console.WriteLine("Good, and " + noOfRows + " wide. Now, let's see, your square should look something like this: \n");

            for (int i = 0; i < noOfColumns; i++)
            {
                for (int j = 0; j < noOfRows; j++)
                {
                    if (i == 0 || i == (noOfColumns - 1) || j == 0 || j == (noOfRows - 1))
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.Write('\n');
            }
        }

        static int ValidateNumberInput()
        {
            int temp;
            bool isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            while (!isANumber)
            {
                Console.WriteLine("I am sorry, that is not a valid number. Could you please enter a valid number (no decimals)");
                isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            }
            return temp;
        }
    }
}
