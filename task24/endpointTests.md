GET: https://localhost:44348/api
[
    {
        "id": 1,
        "name": "Bighorn Steakhouse",
        "address": "Strandgata 15, Gjøvik",
        "stars": 5,
        "reviewerName": "Jens"
    },
    {
        "id": 2,
        "name": "Nishiki Sushi",
        "address": "Stovner Senter 3, Oslo",
        "stars": 4,
        "reviewerName": "Anonymous"
    },
    {
        "id": 3,
        "name": "Shawarma House",
        "address": "Smalgangen 1, Oslo",
        "stars": 4,
        "reviewerName": "Petter"
    }
]

GET: https://localhost:44348/api/2
{
    "id": 2,
    "name": "Nishiki Sushi",
    "address": "Stovner Senter 3, Oslo",
    "stars": 4,
    "reviewerName": "Anonymous"
}

GET: https://localhost:44348/api/stars/4
[
    {
        "id": 2,
        "name": "Nishiki Sushi",
        "address": "Stovner Senter 3, Oslo",
        "stars": 4,
        "reviewerName": "Anonymous"
    },
    {
        "id": 3,
        "name": "Shawarma House",
        "address": "Smalgangen 1, Oslo",
        "stars": 4,
        "reviewerName": "Petter"
    }
]

POST: https://localhost:44348/api/
{
    "name": "Burger king",
    "address": "Jernbanetorget 1, Oslo",
    "stars": 1,
    "reviewerName": "Ronald MacDonald"
}
	
PUT: https://localhost:44348/api/3
{
    "id": 3,
    "name": "House",
    "address": "Smalgangen 1, Oslo",
    "stars": 2,
    "reviewerName": ""
}
	
DELETE: https://localhost:44348/api/1