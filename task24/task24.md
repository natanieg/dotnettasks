# Assignment 24:  Yülp¿ Review API
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task24

# Assignment text
- Build a REST api for storing your personal reviews of bars/restaurants/tea houses.
- Each bar/restaurant/tea house should have the following attributes:
	- Name
	- Address
	- Stars (rating)
	- Reviewer Name (Optional)

-A blank reviewer name must be replaced with "Anonymous".
-Yout api must allow for the following endpoints:
	- GET /api/{restaurant_id}
   	- POST /api/{restaurant_id}
   	- PUT /api/{restaurant_id}
   	- DELETE /api/{restaurant_id}
   	- GET /api/stars/{number_of_stars}
   	- GET /api/reviewer/{reviewer_name}

- INCLUDE A TXT/.md/something file with appropriate endpoint tests

- OPTIONAL:
- GET /api/anon
	- returns anonymous reviews
	

# Endpoint tests:
GET: https://localhost:44348/api
[
    {
        "id": 1,
        "name": "Bighorn Steakhouse",
        "address": "Strandgata 15, Gjøvik",
        "stars": 5,
        "reviewerName": "Jens"
    },
    {
        "id": 2,
        "name": "Nishiki Sushi",
        "address": "Stovner Senter 3, Oslo",
        "stars": 4,
        "reviewerName": "Anonymous"
    },
    {
        "id": 3,
        "name": "Shawarma House",
        "address": "Smalgangen 1, Oslo",
        "stars": 4,
        "reviewerName": "Petter"
    }
]

GET: https://localhost:44348/api/2
{
    "id": 2,
    "name": "Nishiki Sushi",
    "address": "Stovner Senter 3, Oslo",
    "stars": 4,
    "reviewerName": "Anonymous"
}

GET: https://localhost:44348/api/stars/4
[
    {
        "id": 2,
        "name": "Nishiki Sushi",
        "address": "Stovner Senter 3, Oslo",
        "stars": 4,
        "reviewerName": "Anonymous"
    },
    {
        "id": 3,
        "name": "Shawarma House",
        "address": "Smalgangen 1, Oslo",
        "stars": 4,
        "reviewerName": "Petter"
    }
]

GET: https://localhost:44348/api/reviewer/Jens
[
    {
        "id": 1,
        "name": "Bighorn Steakhouse",
        "address": "Strandgata 15, Gjøvik",
        "stars": 5,
        "reviewerName": "Jens"
    }
]

GET: https://localhost:44348/api/anon
[
    {
        "id": 2,
        "name": "Nishiki Sushi",
        "address": "Stovner Senter 3, Oslo",
        "stars": 4,
        "reviewerName": "Anonymous"
    }
]

POST: https://localhost:44348/api/
{
    "name": "Burger king",
    "address": "Jernbanetorget 1, Oslo",
    "stars": 1,
    "reviewerName": "Ronald MacDonald"
}
	
PUT: https://localhost:44348/api/3
{
    "id": 3,
    "name": "House",
    "address": "Smalgangen 1, Oslo",
    "stars": 2,
    "reviewerName": ""
}
	
DELETE: https://localhost:44348/api/1
