﻿using Microsoft.EntityFrameworkCore;

namespace task24.Models
{
    public class ResturantContext : DbContext
    {
        public ResturantContext(DbContextOptions<ResturantContext> options) : base(options)
        {

        }

        public DbSet<Resturant> Resturant { get; set; }
    }
}
