﻿namespace task24.Models
{
    public class Resturant
    {
        public long ID { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public int stars { get; set; }
        public string reviewerName { get; set; }
    }
}
