﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using task24.Models;

namespace task24.Controllers
{
    [Route("api")]
    [ApiController]
    public class ResturantController : ControllerBase
    {
        private readonly ResturantContext _context;

        public ResturantController(ResturantContext context)
        {
            _context = context;

            if (_context.Resturant.Count() == 0)
            {
                //  Add sample resturants if none exists, as to never have an empty collection
                _context.Resturant.Add(new Resturant { name = "Bighorn Steakhouse", address = "Strandgata 15, Gjøvik", stars = 5, reviewerName = "Jens" });
                _context.Resturant.Add(new Resturant { name = "Nishiki Sushi", address = "Stovner Senter 3, Oslo", stars = 4, reviewerName = "Anonymous" });
                _context.Resturant.Add(new Resturant { name = "Shawarma House", address = "Smalgangen 1, Oslo", stars = 4, reviewerName = "Petter" });
                _context.SaveChanges();
            }
        }

        // GET: api
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Resturant>>> GetResturants()
        {
            return await _context.Resturant.ToListAsync();
        }

        // GET: api/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Resturant>> GetResturant(long id)
        {
            var resturant = await _context.Resturant.FindAsync(id);

            if (resturant == null)
            {
                return NotFound();
            }
            return resturant;
        }

        // GET: api/stars/{stars}
        [HttpGet("stars/{stars}")]
        public async Task<ActionResult<IEnumerable<Resturant>>> GetResturantByStars(int stars)
        {
            if (stars <= 0)
            {
                BadRequest();
            }

            return await _context.Resturant.Where<Resturant>(item => item.stars == stars).ToListAsync();
        }

        // GET: api/reviewer/reviewerName
        [HttpGet("reviewer/{reviewerName}")]
        public async Task<ActionResult<IEnumerable<Resturant>>> GetResturantByReviewer(string reviewerName)
        {
            return await _context.Resturant.Where<Resturant>(item => reviewerName.Equals(item.reviewerName)).ToListAsync();
        }

        //GET: api/anon
        [HttpGet("anon")]
        public async Task<ActionResult<IEnumerable<Resturant>>> GetAllAnonymousReviews()
        {
            return await _context.Resturant.Where<Resturant>(item => item.reviewerName.Equals("Anonymous")).ToListAsync();
        }

        // POST api
        [HttpPost]
        public async Task<ActionResult<Resturant>> CreateNewReview(Resturant resturant)
        {
            if (resturant.reviewerName == "" ||
                resturant.reviewerName == null)
            {
                resturant.reviewerName = "Anonymous";
            }

            _context.Resturant.Add(resturant);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(Resturant), new { id = resturant.ID }, resturant);
        }

        // PUT api/5
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateReview(long id, Resturant resturant)
        {
            if (resturant.reviewerName == "" ||
                resturant.reviewerName == null)
            {
                resturant.reviewerName = "Anonymous";
            }

            if (id != resturant.ID || resturant == null)
            {
                return BadRequest();
            }

            _context.Entry(resturant).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE api/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteReview(long id)
        {
            var resturant = await _context.Resturant.FindAsync(id);

            if (resturant == null)
            {
                return NotFound();
            }

            _context.Resturant.Remove(resturant);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
