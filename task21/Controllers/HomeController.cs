﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using task21.Models;

namespace task21.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index(string choice)
        {
            PictureModel picture = new PictureModel();
            Picture pic = new Picture();

            switch (choice)
            {
                case "Dog":
                    pic = picture.GetDog();
                    break;

                case "Cat":
                    pic = picture.GetCat();
                    break;

                default:
                    pic = picture.GetRandom();
                    break;
            }
            return View(pic);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
