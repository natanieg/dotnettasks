# Assignment 21:  Cats and dogs
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task21

# Assignment text
Build an app.

It must have two buttons, [CAT] and [DOG].

When the user presses either button an image of the relevant animal is loaded from the APIs. 
(Before the user presses a button it can show any image you chose.)