﻿using Newtonsoft.Json;
using System.IO;
using System.Net;

namespace task21.Models
{
    public class PictureModel
    {
        public Picture GetDog()
        {
            string jsonDog;
            Picture picObj;
            do
            {
                using (WebClient wc = new WebClient())
                {
                    jsonDog = wc.DownloadString("https://random.dog/woof.json");
                }
                picObj = JsonConvert.DeserializeObject<Picture>(jsonDog);
            } while (!ValidDisplayType(picObj.url));

            picObj.source = picObj.url;
            return picObj;
        }

        public Picture GetCat()
        {
            string jsonCat;
            Picture picObj;
            do
            {
                using (WebClient wc = new WebClient())
                {
                    jsonCat = wc.DownloadString("https://aws.random.cat/meow");
                }
                picObj = JsonConvert.DeserializeObject<Picture>(jsonCat);
            } while (!ValidDisplayType(picObj.file));

            picObj.source = picObj.file;
            return picObj;
        }

        public Picture GetRandom()
        {
            Picture picObj = new Picture();
            picObj.source = @"https://picsum.photos/536/354";
            return picObj;
        }

        private bool ValidDisplayType(string path)
        {
            if (Path.GetExtension(path).Equals(".mp4") ||
                Path.GetExtension(path).Equals(".webm"))
            {
                return false;
            }
            return true;
        }
    }
}
