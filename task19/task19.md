# Assignment 19: JSON (and some DB practice)
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task19

# Assignment text
• Install-Package newtonsoft.json
• Download the Chinook DB:
	- https://www.sqlitetutorial.net/sqlite-sample-database/
• Write an app (console is fine) to read from the Customers and invoice tables
• Select a single random customer, then create (save) a single file	based on their name:
	- Ie: Craig_Marais.json
	- This file must contain their information AND a list of invoices associated with them.