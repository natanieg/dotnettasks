﻿using System;
using System.Data.SQLite;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace task19
{
    class Program
    {
        static void Main(string[] args)
        {
            SQLiteConnection conn = new SQLiteConnection("DataSource = ../../../chinook.db; Version = 3; New = True; Compress =True; ");
            List<int> customerIDList = new List<int>();

            try
            {
                Console.WriteLine("Attempting to connect to chinook...");
                conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                conn.Close();
            }

            SQLiteDataReader sqliteDataReaderID;
            SQLiteCommand sqliteCmdID;
            sqliteCmdID = conn.CreateCommand();
            sqliteCmdID.CommandText =
                    "SELECT CustomerId " +
                    "FROM customers ";
            sqliteDataReaderID = sqliteCmdID.ExecuteReader();

            while (sqliteDataReaderID.Read())
            {
                customerIDList.Add((int)(long)sqliteDataReaderID["CustomerId"]);
            }

            Random rand = new Random();
            int customerIdIndex = rand.Next(customerIDList.Count);

            SQLiteDataReader sqliteDataReaderCustomer;
            SQLiteCommand sqliteCmdCustomer;
            sqliteCmdCustomer = conn.CreateCommand();
            sqliteCmdCustomer.CommandText =
                   "SELECT CustomerId, FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email " +
                    "FROM customers " +
                    "WHERE CustomerId = " + customerIDList[customerIdIndex] + " ";
            sqliteDataReaderCustomer = sqliteCmdCustomer.ExecuteReader();

            Customer customer = new Customer();
            while (sqliteDataReaderCustomer.Read())
            {
                customer.customerID = (int)(long)sqliteDataReaderCustomer["CustomerId"];
                customer.firstName = sqliteDataReaderCustomer["FirstName"].ToString();
                customer.lastName = sqliteDataReaderCustomer["LastName"].ToString();
                customer.company = sqliteDataReaderCustomer["Company"].ToString();
                customer.address = sqliteDataReaderCustomer["Address"].ToString();
                customer.city = sqliteDataReaderCustomer["City"].ToString();
                customer.state = sqliteDataReaderCustomer["State"].ToString();
                customer.country = sqliteDataReaderCustomer["Country"].ToString();
                customer.postalCode = sqliteDataReaderCustomer["PostalCode"].ToString();
                customer.phoneNumber = sqliteDataReaderCustomer["Phone"].ToString();
                customer.fax = sqliteDataReaderCustomer["Fax"].ToString();
                customer.email = sqliteDataReaderCustomer["Email"].ToString();
            }

            SQLiteDataReader sqliteDataReaderInvoice;
            SQLiteCommand sqliteCmdInvoice;
            sqliteCmdInvoice = conn.CreateCommand();
            sqliteCmdInvoice.CommandText =
                   "SELECT InvoiceId, InvoiceDate, BillingAddress, BillingCity " +
                    "FROM invoices " +
                    "WHERE CustomerId = " + customer.customerID + " ";
            sqliteDataReaderInvoice = sqliteCmdInvoice.ExecuteReader();
            while (sqliteDataReaderInvoice.Read())
            {
                Invoice invoice = new Invoice();
                invoice.ID = (int)(long)sqliteDataReaderInvoice["InvoiceId"];
                invoice.date = (DateTime)sqliteDataReaderInvoice["InvoiceDate"];
                invoice.billingAddress = sqliteDataReaderInvoice["BillingAddress"].ToString();
                invoice.billingCity = sqliteDataReaderInvoice["BillingCity"].ToString();
                customer.invoiceList.Add(invoice);
            }

            string JSONString = JsonConvert.SerializeObject(customer, Formatting.Indented);
            Console.WriteLine("Writing customer " + customer.firstName + " " + customer.lastName + " to a file in the 'customers' folder. ");
            System.IO.File.WriteAllText(@"../../../customers/" + customer.firstName + "_" + customer.lastName + ".json", JSONString);

            conn.Close();
        }
    }
}
