﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task19
{
    class Customer
    {
        public int customerID;
        public string firstName;
        public string lastName;
        public string company;
        public string address;
        public string city;
        public string state;
        public string country;
        public string postalCode;
        public string phoneNumber;
        public string fax;
        public string email;
        public List<Invoice> invoiceList = new List<Invoice>();
    }
}
