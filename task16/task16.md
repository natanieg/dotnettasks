# Assignment 16: A more realistic GUI – RPG
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task16

# Assignment text
- Write a program which allows a user to create an RPG character – Use WinForms or WPF to create a UI
	• A character can be a Wizard, Warrior, Thief
	• Each character type can have subtypes (be creative)
	• A character has HP, Mana/Energy, Name, armorRating
	• A character can attack and move
	• Once the user clicks create/submit, a summary of the character must be displayed
	• This character must be created as an object and used to display the summary as well as write the details to a text file
	
- Create the class structure first
	• Build a DLL of this (NO GUI)
	• Choose a partner
	• Send each other your DLL
	• Use their DLL to create objects, therefore build your UI according to their class structure