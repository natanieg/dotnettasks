﻿namespace task16Stage2
{
    partial class FormCharacterCreation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.labelHP = new System.Windows.Forms.Label();
            this.labelMP = new System.Windows.Forms.Label();
            this.labelAC = new System.Windows.Forms.Label();
            this.labelAttack = new System.Windows.Forms.Label();
            this.labelMovement = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbAttack = new System.Windows.Forms.TextBox();
            this.labelSubclass = new System.Windows.Forms.Label();
            this.labelClass = new System.Windows.Forms.Label();
            this.dropdownClass = new System.Windows.Forms.ComboBox();
            this.dropdownSubclass = new System.Windows.Forms.ComboBox();
            this.numHP = new System.Windows.Forms.NumericUpDown();
            this.numAC = new System.Windows.Forms.NumericUpDown();
            this.numMP = new System.Windows.Forms.NumericUpDown();
            this.buttonCreateCharacter = new System.Windows.Forms.Button();
            this.listViewCharacter = new System.Windows.Forms.ListView();
            this.numMovement = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMovement)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(24, 40);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(113, 17);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Character name:";
            // 
            // labelHP
            // 
            this.labelHP.AutoSize = true;
            this.labelHP.Location = new System.Drawing.Point(24, 164);
            this.labelHP.Name = "labelHP";
            this.labelHP.Size = new System.Drawing.Size(126, 17);
            this.labelHP.TabIndex = 1;
            this.labelHP.Text = "HP (health points):";
            // 
            // labelMP
            // 
            this.labelMP.AutoSize = true;
            this.labelMP.Location = new System.Drawing.Point(24, 200);
            this.labelMP.Name = "labelMP";
            this.labelMP.Size = new System.Drawing.Size(123, 17);
            this.labelMP.TabIndex = 2;
            this.labelMP.Text = "MP (mana points):";
            // 
            // labelAC
            // 
            this.labelAC.AutoSize = true;
            this.labelAC.Location = new System.Drawing.Point(24, 236);
            this.labelAC.Name = "labelAC";
            this.labelAC.Size = new System.Drawing.Size(126, 17);
            this.labelAC.TabIndex = 3;
            this.labelAC.Text = "AC (Armour class):";
            // 
            // labelAttack
            // 
            this.labelAttack.AutoSize = true;
            this.labelAttack.Location = new System.Drawing.Point(24, 280);
            this.labelAttack.Name = "labelAttack";
            this.labelAttack.Size = new System.Drawing.Size(88, 17);
            this.labelAttack.TabIndex = 4;
            this.labelAttack.Text = "Main attack: ";
            // 
            // labelMovement
            // 
            this.labelMovement.AutoSize = true;
            this.labelMovement.Location = new System.Drawing.Point(24, 323);
            this.labelMovement.Name = "labelMovement";
            this.labelMovement.Size = new System.Drawing.Size(120, 17);
            this.labelMovement.TabIndex = 5;
            this.labelMovement.Text = "Movement speed:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(178, 40);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 22);
            this.tbName.TabIndex = 0;
            // 
            // tbAttack
            // 
            this.tbAttack.Location = new System.Drawing.Point(178, 280);
            this.tbAttack.Name = "tbAttack";
            this.tbAttack.Size = new System.Drawing.Size(100, 22);
            this.tbAttack.TabIndex = 6;
            this.tbAttack.TextChanged += new System.EventHandler(this.TbAttack_TextChanged);
            // 
            // labelSubclass
            // 
            this.labelSubclass.AutoSize = true;
            this.labelSubclass.Location = new System.Drawing.Point(24, 115);
            this.labelSubclass.Name = "labelSubclass";
            this.labelSubclass.Size = new System.Drawing.Size(69, 17);
            this.labelSubclass.TabIndex = 12;
            this.labelSubclass.Text = "Subclass:";
            // 
            // labelClass
            // 
            this.labelClass.AutoSize = true;
            this.labelClass.Location = new System.Drawing.Point(24, 77);
            this.labelClass.Name = "labelClass";
            this.labelClass.Size = new System.Drawing.Size(110, 17);
            this.labelClass.TabIndex = 14;
            this.labelClass.Text = "Character class:";
            // 
            // dropdownClass
            // 
            this.dropdownClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropdownClass.FormattingEnabled = true;
            this.dropdownClass.Location = new System.Drawing.Point(178, 77);
            this.dropdownClass.Name = "dropdownClass";
            this.dropdownClass.Size = new System.Drawing.Size(121, 24);
            this.dropdownClass.TabIndex = 1;
            this.dropdownClass.SelectedIndexChanged += new System.EventHandler(this.DropdownClass_SelectedIndexChanged);
            // 
            // dropdownSubclass
            // 
            this.dropdownSubclass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropdownSubclass.FormattingEnabled = true;
            this.dropdownSubclass.Location = new System.Drawing.Point(178, 115);
            this.dropdownSubclass.Name = "dropdownSubclass";
            this.dropdownSubclass.Size = new System.Drawing.Size(121, 24);
            this.dropdownSubclass.TabIndex = 2;
            // 
            // numHP
            // 
            this.numHP.Location = new System.Drawing.Point(178, 164);
            this.numHP.Name = "numHP";
            this.numHP.Size = new System.Drawing.Size(120, 22);
            this.numHP.TabIndex = 3;
            // 
            // numAC
            // 
            this.numAC.Location = new System.Drawing.Point(178, 236);
            this.numAC.Name = "numAC";
            this.numAC.Size = new System.Drawing.Size(120, 22);
            this.numAC.TabIndex = 5;
            // 
            // numMP
            // 
            this.numMP.Location = new System.Drawing.Point(178, 200);
            this.numMP.Name = "numMP";
            this.numMP.Size = new System.Drawing.Size(120, 22);
            this.numMP.TabIndex = 4;
            // 
            // buttonCreateCharacter
            // 
            this.buttonCreateCharacter.Location = new System.Drawing.Point(27, 382);
            this.buttonCreateCharacter.Name = "buttonCreateCharacter";
            this.buttonCreateCharacter.Size = new System.Drawing.Size(251, 43);
            this.buttonCreateCharacter.TabIndex = 21;
            this.buttonCreateCharacter.Text = "Create character";
            this.buttonCreateCharacter.UseVisualStyleBackColor = true;
            this.buttonCreateCharacter.Click += new System.EventHandler(this.ButtonCreateCharacter_Click);
            // 
            // listViewCharacter
            // 
            this.listViewCharacter.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.listViewCharacter.HideSelection = false;
            this.listViewCharacter.Location = new System.Drawing.Point(384, 35);
            this.listViewCharacter.Name = "listViewCharacter";
            this.listViewCharacter.Scrollable = false;
            this.listViewCharacter.Size = new System.Drawing.Size(349, 305);
            this.listViewCharacter.TabIndex = 22;
            this.listViewCharacter.UseCompatibleStateImageBehavior = false;
            // 
            // numMovement
            // 
            this.numMovement.Location = new System.Drawing.Point(179, 318);
            this.numMovement.Name = "numMovement";
            this.numMovement.Size = new System.Drawing.Size(120, 22);
            this.numMovement.TabIndex = 7;
            // 
            // FormCharacterCreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 521);
            this.Controls.Add(this.numMovement);
            this.Controls.Add(this.listViewCharacter);
            this.Controls.Add(this.buttonCreateCharacter);
            this.Controls.Add(this.numMP);
            this.Controls.Add(this.numAC);
            this.Controls.Add(this.numHP);
            this.Controls.Add(this.dropdownSubclass);
            this.Controls.Add(this.dropdownClass);
            this.Controls.Add(this.labelClass);
            this.Controls.Add(this.labelSubclass);
            this.Controls.Add(this.tbAttack);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.labelMovement);
            this.Controls.Add(this.labelAttack);
            this.Controls.Add(this.labelAC);
            this.Controls.Add(this.labelMP);
            this.Controls.Add(this.labelHP);
            this.Controls.Add(this.labelName);
            this.Name = "FormCharacterCreation";
            this.Text = "Character creation";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMovement)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelHP;
        private System.Windows.Forms.Label labelMP;
        private System.Windows.Forms.Label labelAC;
        private System.Windows.Forms.Label labelAttack;
        private System.Windows.Forms.Label labelMovement;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbAttack;
        private System.Windows.Forms.Label labelSubclass;
        private System.Windows.Forms.Label labelClass;
        private System.Windows.Forms.ComboBox dropdownClass;
        private System.Windows.Forms.ComboBox dropdownSubclass;
        private System.Windows.Forms.NumericUpDown numHP;
        private System.Windows.Forms.NumericUpDown numAC;
        private System.Windows.Forms.NumericUpDown numMP;
        private System.Windows.Forms.Button buttonCreateCharacter;
        private System.Windows.Forms.ListView listViewCharacter;
        private System.Windows.Forms.NumericUpDown numMovement;
    }
}

