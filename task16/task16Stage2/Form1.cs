﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using RPGCharacterGenerator;
using System.IO;
using System.Diagnostics;

namespace task16Stage2
{
    public partial class FormCharacterCreation : Form
    {
        Character character;
        List<string> subclasses;
        List<string> characterClasses = new List<string>();
        List<Type> characterClassType = new List<Type>();
        List<Type> characterClassSubtype;

        public FormCharacterCreation()
        {
            InitializeComponent();
            InitializeClassDropdown();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void InitializeClassDropdown()
        {
            var classTypes = Assembly
                .GetAssembly(typeof(Character))
                .GetTypes()
                .Where(t => t.BaseType == (typeof(Character)));

            foreach (Type characterClass in classTypes)
            {
                characterClasses.Add(characterClass.Name);
                characterClassType.Add(characterClass);
            }

            this.dropdownClass.DataSource = characterClasses;
        }

        private void DropdownClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = dropdownClass.SelectedIndex;
            Type parent = characterClassType[index];//Type.GetType("RPGCharacterGenerator.Thief"); 

            var subclassTypes = Assembly
                .GetAssembly(typeof(Character))
                .GetTypes()
                .Where(t => t.BaseType == parent);

            subclasses = new List<string>();
            characterClassSubtype = new List<Type>();
            foreach (Type subclass in subclassTypes)
            {
                subclasses.Add(subclass.Name);
                characterClassSubtype.Add(subclass);
            }

            this.dropdownSubclass.DataSource = subclasses;
        }
        private void ButtonCreateCharacter_Click(object sender, EventArgs e)
        {
            Type characterClass = characterClassSubtype[dropdownSubclass.SelectedIndex];

            character = (Character)Activator.CreateInstance(characterClass,
                (int)numHP.Value, (int)numMP.Value, tbName.Text, (int)numAC.Value, tbAttack.Text, (int)numMovement.Value);


            DisplayCharacter(character);
            SaveCharacterToFile(character);
        }

        private void DisplayCharacter(Character character)
        {
            listViewCharacter.View = View.Tile;
            listViewCharacter.Columns.Add("File type", 250, HorizontalAlignment.Left);

            /*
            listViewCharacter.Items.Add("Name: " + tbName.Text);
            listViewCharacter.Items.Add("Class: " + dropdownClass.Text);
            listViewCharacter.Items.Add("Subclass: " + dropdownSubclass.Text);
            listViewCharacter.Items.Add("HP: " + numHP.Value);
            listViewCharacter.Items.Add("MP: " + numMP.Value);
            listViewCharacter.Items.Add("AC: " + numAC.Value);
            listViewCharacter.Items.Add("Main attack: " + tbAttack.Text);
            listViewCharacter.Items.Add("Movement speed: " + numMovement.Value);
            */

            listViewCharacter.Items.Add("Name: " + character.Name);
            listViewCharacter.Items.Add("Class: " + character.GetClass());
            //listViewCharacter.Items.Add("Subclass: " + character.GetClass());
            listViewCharacter.Items.Add("HP: " + character.HP);
            listViewCharacter.Items.Add("MP: " + character.MP);
            listViewCharacter.Items.Add("AC: " + character.AC);
            listViewCharacter.Items.Add("Main attack: " + character.Attack);
            listViewCharacter.Items.Add("Movement speed: " + character.Movement);

        }

        public void SaveCharacterToFile(Character character)
        {

            const string filepath = @"..\..\..\savedCharacter\character.txt";
            using (var file = new StreamWriter(filepath))
            {
                /*
                file.WriteLine(numHP.Value);
                file.WriteLine(numMP.Value);
                file.WriteLine(tbName.Text);
                file.WriteLine(numAC.Value);
                file.WriteLine(tbAttack.Text);
                file.WriteLine(numMovement.Value);

                file.WriteLine(dropdownClass.Text);
                file.WriteLine(dropdownSubclass.Text);
                */
                file.WriteLine(character.HP);
                file.WriteLine(character.MP);
                file.WriteLine(character.Name);
                file.WriteLine(character.AC);
                file.WriteLine(character.Attack);
                file.WriteLine(character.Movement);

                file.WriteLine(character.GetClass());

                file.Close();

                Process.Start(filepath);
            }
        }
    }
}
