﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task16
{
    public class Mercenary : Warrior
    {

        public Mercenary(int hp, int mp, string characterName, int ac, string characterAttack, int characterMovement) :
            base(hp, mp, characterName, ac, characterAttack, characterMovement)
        {

        }
        public string[] mercenaryAbilities { get; set; }
    }
}
