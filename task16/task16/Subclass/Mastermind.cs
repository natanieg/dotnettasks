﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task16
{
    public class Mastermind : Thief
    {

        public Mastermind(int hp, int mp, string characterName, int ac, string characterAttack, int characterMovement) :
            base(hp, mp, characterName, ac, characterAttack, characterMovement)
        {

        }
        public string[] mastermindAbilities { get; set; }
    }
}
