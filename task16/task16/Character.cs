﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task16
{
    ///==========================================
    /// Title:  Task 16: A more realistic GUI – RPG
    /// Author: Nataniel Gåsøy
    /// Date:   27-Aug-2019
    ///==========================================

    public abstract class Character
    {
        public int HP { get; set; }
        public int MP { get; set; }
        public string name { get; set; }
        public int AC { get; set; }
        public string attack { get; set; }
        public int movement { get; set; }


        public Character(int hp, int mp, string characterName, int ac, string characterAttack, int characterMovement)
        {
            HP = hp;
            MP = mp;
            name = characterName;
            AC = ac;
            attack = characterAttack;
            movement = characterMovement;

        }
        public string GetClass()
        {
            return GetType().Name;
        }
    }
}
