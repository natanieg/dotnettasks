# Assignment 4: Nested Rectangle
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task4

# Assignment text
Write a program which can be used print a rectangle of size chosen by the user at run-time.
The rectangle should have an outer edge and a second inner edge.
There must be one space between in the inner and outer edge. The rectangle can be made of any character that you choose.
(# is probably a good choice) 