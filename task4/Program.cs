﻿using System;

namespace task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int noOfColumns;
            int noOfRows;
            int borderSize = 1;

            #region GetInput
            Console.WriteLine("Hello there, today we are making a square. First, how high should the cube be? ");
            noOfColumns = ValidateNumberInput();

            Console.WriteLine("OK good, it should be " + noOfColumns + " tall, and how wide should it be? ");
            noOfRows = ValidateNumberInput();

            Console.WriteLine("How large should the border between the outer and inner square be? ");
            borderSize = ValidateNumberInput();
            #endregion

            int[,] square = new int[noOfColumns, noOfRows];
            Console.WriteLine("Good, and " + noOfRows + " wide. Now, let's see, your square should look something like this: \n");

            for (int i = 0; i < noOfColumns; i++)
            {
                for (int j = 0; j < noOfRows; j++)
                {
                    if (i == 0 || i == (noOfColumns - 1) || j == 0 || j == (noOfRows - 1)                                   // Outer square
                                                                                                                            /*
                                                                                                                            || (i == 1 + borderSize && j >= 1 + borderSize && j < noOfRows - borderSize - 1)                    // Inner square, top row
                                                                                                                            || (j == 1 + borderSize && i >= 1 + borderSize && i < noOfColumns - borderSize - 1)                 // Inner square, bottom row
                                                                                                                            || (i == noOfColumns - borderSize - 2 && j >= 1 + borderSize && j < noOfRows - borderSize - 1)      // Inner square, left column                                                                                  // Inner square, left column
                                                                                                                            || (j == noOfRows - borderSize - 2 && i >= 1 + borderSize && i < noOfColumns - borderSize - 1))     // Inner square, right column
                                                                                                                            */
                        || ((i == 1 + borderSize || i == noOfRows - borderSize - 2) && (j > borderSize && j < noOfRows - borderSize - 1))           // Horizontal lines of inner square
                        || ((j == 1 + borderSize || j == noOfColumns - borderSize - 2) && (i > borderSize && i < noOfColumns - borderSize - 1)))    // Vertical lines of inner square
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write("|");
                    }
                }
                Console.Write('\n');
            }
        }

        static int ValidateNumberInput()
        {
            int temp;
            bool isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            while (!isANumber)
            {
                Console.WriteLine("I am sorry, that is not a valid number. Could you please enter a valid number (no decimals)");
                isANumber = Int32.TryParse(Console.ReadLine(), out temp);
            }
            return temp;
        }
    }
}
