# Assignment 14: IEnumerable
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task14

# Assignment text
- Extend you Animal class to be:
	- IEnumerable AND IComparable (It must implement the interfaces)
	- Demonstrate that these function correctly in your code (use the Sort() method on your class)