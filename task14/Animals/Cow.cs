﻿using System;
using System.Collections.Generic;
using task13.Interfaces;

namespace task13.Animals
{

    class Cow : Herbivore, IRun, IWalk, ISwim
    {

        public Cow() : base()
        {
            locomotionList.Add(Movement.Run);
            locomotionList.Add(Movement.Swim);
            locomotionList.Add(Movement.Walk);

        }

        public Cow(string name) : this()
        {
            animalName = name;
        }

        public void Run()
        {
            PrintMovement("running");
        }

        public void Swim()
        {
            PrintMovement("swimming");
        }

        public void Walk()
        {
            PrintMovement("walking");
        }

        public void PrintMovement(string move)
        {

            Console.WriteLine(animalName + " is currently " + move + ". \n");
        }
    }
}
