﻿using System;
using System.Collections.Generic;
using task13.Interfaces;

namespace task13.Animals
{
    class Cat : Carnivore, IClimb, IRun, IWalk, ISwim
    {
        public Cat() : base()
        {
            locomotionList.Add(Movement.Climb);
            locomotionList.Add(Movement.Run);
            locomotionList.Add(Movement.Swim);
            locomotionList.Add(Movement.Walk);
        }

        public Cat(string name) : this()
        {
            animalName = name;
        }

        public void Climb()
        {
            PrintMovement("climbing");
        }

        public void Run()
        {
            PrintMovement("running");
        }

        public void Swim()
        {
            PrintMovement("swimming");
        }

        public void Walk()
        {
            PrintMovement("walking");
        }

        public void PrintMovement(string move)
        {

            Console.WriteLine(animalName + " is currently " + move + ". \n");
        }
    }
}
