﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task13.Interfaces
{
    interface IFly : IMove
    {
        void Fly();
    }
}
