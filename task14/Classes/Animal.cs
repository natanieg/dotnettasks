﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using task13.Interfaces;

namespace task13
{
    class Animal : IMove, IEnumerable, IComparable<Animal>
    {
        public string animalName { get; set; }
        public List<Movement> locomotionList = new List<Movement>();

        public Animal()
        {

        }

        public Animal(string name)
        {
            animalName = name;
        }

        public IEnumerator GetEnumerator()
        {
            return locomotionList.GetEnumerator();
        }

        public int CompareTo(Animal other)
        {
            if (other == null)
            {
                return 1;
            }

            return this.GetType().Name.ToString().CompareTo(other.GetType().Name.ToString());
            throw new ArgumentException("Object " + other.GetType().Name.ToString() + " is not of type <Animal>. ");
        }
    }

    enum Movement
    {
        Climb,
        Fly,
        Run,
        Swim,
        Walk,
    }

}
