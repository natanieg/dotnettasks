# Assignment 22:  Cats and dogs, the guessing game
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task22

# Assignment text
THIS MUST BE BUILT USING ASP.NET Core MVC
Use both of the random APIs provided previously (https://aws.random.cat/meow)(https://random.dog/woof.json).

Build a website that randomly switches between pictures from the APIs.

Users must chose if the picture is a CAT or a DOG. If chosen correctly the user gets points. If wrong, then they lose, and must start again.

The website must keep track of the users progress / score.

If the user gets to ELEVEN correct answers then they win. Show a win screen (new page).