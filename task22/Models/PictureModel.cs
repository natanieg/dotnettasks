﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace task22.Models
{
    public class PictureModel
    {
        static GameState game;

        public PictureModel()
        {
            game = new GameState();
        }
        public Picture GetDog()
        {
            string jsonDog;
            Picture picObj;
            do
            {
                using (WebClient wc = new WebClient())
                {
                    jsonDog = wc.DownloadString("https://random.dog/woof.json");
                }
                picObj = JsonConvert.DeserializeObject<Picture>(jsonDog);
            } while (!ValidDisplayType(picObj.url));

            picObj.source = picObj.url;
            return picObj;
        }

        public Picture GetCat()
        {
            string jsonCat;
            Picture picObj;
            do
            {
                using (WebClient wc = new WebClient())
                {
                    jsonCat = wc.DownloadString("https://aws.random.cat/meow");
                }
                picObj = JsonConvert.DeserializeObject<Picture>(jsonCat);
            } while (!ValidDisplayType(picObj.file));

            picObj.source = picObj.file;
            return picObj;
        }

        public Picture GetRandom()
        {
            string json;
            Picture picObj = GetStockPhoto();
            Random rand = new Random();
            int current = rand.Next(0, 2);  //0 = dog, 1 = cat

            switch (current)
            {
                case 0:
                    do
                    {
                        using (WebClient wc = new WebClient())
                        {
                            try
                            {
                                json = wc.DownloadString("https://random.dog/woof.json");
                            }
                            catch (WebException exp)
                            {
                                throw new Exception(exp.Message);
                            }
                        }
                        picObj = JsonConvert.DeserializeObject<Picture>(json);
                    } while (!ValidDisplayType(picObj.url));

                    picObj.source = picObj.url;
                    game.currentAnswer = "Dog";
                    picObj.currentAnimal = "Dog";
                    break;

                case 1:
                    do
                    {
                        using (WebClient wc = new WebClient())
                        {
                            try
                            {
                                json = wc.DownloadString("https://aws.random.cat/meow");
                            }
                            catch (WebException exp)
                            {
                                throw new Exception(exp.Message);
                            }
                        }
                        picObj = JsonConvert.DeserializeObject<Picture>(json);
                    } while (!ValidDisplayType(picObj.file));

                    picObj.source = picObj.file;
                    game.currentAnswer = "Cat";
                    picObj.currentAnimal = "Cat";
                    break;
            }
            return picObj;
        }

        public Picture GetStockPhoto()
        {
            Picture picObj = new Picture();
            picObj.source = @"https://picsum.photos/536/354";
            return picObj;
        }

        private bool ValidDisplayType(string path)
        {
            if (Path.GetExtension(path).Equals(".mp4") ||
                Path.GetExtension(path).Equals(".webm"))
            {
                return false;
            }
            return true;
        }
    }
}
