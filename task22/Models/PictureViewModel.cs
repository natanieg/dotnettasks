﻿namespace task22.Models
{
    public class Picture
    {
        public string source { get; set; }
        public string url { get; set; }
        public string file { get; set; }
        public string currentAnimal { get; set; }
    }
}
