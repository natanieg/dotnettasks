﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task22.Models
{
    public class GameState
    {
        public int winningScore { get; set; }
        public int correctAnswerCount { get; set; }
        public string currentAnswer { get; set; }
    }
}
