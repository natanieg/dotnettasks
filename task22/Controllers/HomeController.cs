﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using task22.Models;

namespace task22.Controllers
{
    public class HomeController : Controller
    {
        private static GameState game;

        public IActionResult Index(string choice, bool existingGame)
        {
            GameStateModel gameModel = new GameStateModel();
            PictureModel picture = new PictureModel();
            Picture pic = new Picture();

            if (!existingGame)
            {
                game = new GameState();
                game.currentAnswer = "";
                game.correctAnswerCount = 0;
            }
            if (choice == null)
            {
                choice = "None";
            }

            try
            {
                pic = picture.GetRandom();
            }
            catch (Exception exp)
            {
                return View(exp.Message);
            }

            //  Checks if current answer is actually an answer, 
            //  to ensure the player has actually answered wrong instead of the system starting a new game
            if (game.currentAnswer.Equals("") ||
                gameModel.CheckAnswer(game, choice))
            {

                game.currentAnswer = pic.currentAnimal;
                ViewData["Score"] = game.correctAnswerCount;
            }
            else
            {
                return View("Lose");
            }

            if (game.correctAnswerCount >= 11)
            {
                return View("Win");
            }
            return View(pic);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
