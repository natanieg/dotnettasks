﻿using System;
using System.Collections.Generic;

namespace task10
{
    class Program
    {
        static void Main(string[] args)
        {
            string searchName;
            List<Person> contacts = new List<Person>();
            List<string> contactMatches = new List<string>();
            PopulateContactList(contacts);

            Console.WriteLine("Hello there, who would you like to search for today?: ");
            searchName = Console.ReadLine();

            foreach (Person contact in contacts)
            {
                if (contact.GetFullName().ToLower().Contains(searchName.ToLower()))
                {
                    contactMatches.Add(contact.GetFullName());
                }
            }

            if (contactMatches != null && contactMatches.Count > 0)
            {
                Console.WriteLine("The current contacts matching your search query: ");
                foreach (string contact in contactMatches)
                {
                    Console.WriteLine("      - " + contact);
                }
            }
            else
            {
                Console.WriteLine("I am sorry, but there does not seem to be any contacts by that name");
            }
        }

        static void PopulateContactList(List<Person> list)
        {
            Person johnWick = new Person("Jonathan", "Wick", 12345678);
            Person johnAdams = new Person("John", "Adams", 30081735);
            Person alexanderHamilton = new Person("Alexander", "Hamilton", 111011757);
            Person thomasJefferson = new Person("Thomas", "Jefferson", 13041743);
            Person herculesMulligan = new Person("Hercules", "Mulligan", 25091740);
            Person laFayette = new Person("Marquis De", "La Fayette", 06091757);
            Person christopherRobin = new Person("Christopher", "Robin", 19261926);

            list.Add(johnWick);
            list.Add(johnAdams);
            list.Add(alexanderHamilton);
            list.Add(thomasJefferson);
            list.Add(herculesMulligan);
            list.Add(laFayette);
            list.Add(christopherRobin);
        }
    }
}
