﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task10
{
    class Person
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int phoneNumber { get; set; }

        public Person()
        {

        }

        public Person(string firstName)
        {
            this.firstName = firstName;
        }

        public Person(string firstName, string lastName) : this(firstName)
        {
            this.lastName = lastName;
        }

        public Person(string firstName, string lastName, int phoneNumber) : this(firstName, lastName)
        {
            this.phoneNumber = phoneNumber;
        }

        public string GetFullName()
        {
            return firstName + " " + lastName;
        }
    }
}
