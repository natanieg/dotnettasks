# Assignment 10: Upgraded Name Search
- Link: https://gitlab.com/natanieg/dotnettasks/tree/master/task10

# Assignment text
• Modify your name search solution to have the following:
• A separate class that describes a person
	• Reminder: It will need it’s own source file
	• First Name, Last Name, Telephone, etc
	• Use overloaded constructors*
• Create a collection of this new class
• Modify your search to access a public method within this class